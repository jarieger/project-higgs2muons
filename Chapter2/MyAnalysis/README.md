# MyAnalysis - High-energy Physics Data Root Analyzer

This C++ code is designed to analyze high-energy physics data, specifically related to muons and jets, for the ROOT data analysis framework. It contains a function called `Loop()` that is part of a class called `MyAnalysis`. The code already shows a few examples how to apply selections, perform calculations such as the invariant mass of two muons and pT asymmetry of jets, and fills histograms accordingly.

## How to Use

There are two options to run the code:

1. Run it via `RunAnalysis.C` script. 
2. Run it interactively in a ROOT session.

### Running using `RunAnalysis.C`

To execute this code using the `RunAnalysis.C` script, just run the script in your terminal. The `RunAnalysis.C` script will take care of executing the `MyAnalysis::Loop()` function. Just type `root RunAnalysis.C` in your terminal.

### Running interactively in a ROOT session

To run the code interactively:

1. Open a ROOT session in your terminal.
2. Load the `MyAnalysis.C` file by typing `.L MyAnalysis.C`.
3. Create a `MyAnalysis` object by typing `MyAnalysis t`.
4. Fill the data members of the object by entering `t.GetEntry(12);`, where `12` is the entry number you want to fill.
5. Show the values of a specific entry by typing `t.Show();` for the entry loaded in step 4 or `t.Show(16);` for entry number 16.
6. Run the loop on all entries by typing `t.Loop();`.

## How to Modify the Code

The code begins with initializing variables, 4-momentum vectors, and histograms. You can add more histograms in the section marked "ADD YOUR HISTOGRAMS HERE".

It then loops through all entries in the ROOT data file. To apply user-defined selection criteria, modify the section marked "APPLY YOUR SELECTIONS HERE".

Perform additional calculations in the section marked "PERFORM YOUR ANALYSIS HERE".

Fill your histograms in the section marked "FILL YOUR HISTOGRAMS HERE".

The code calculates the weight for each processID and scales the histograms accordingly. Do not change that part unless you know what you are doing.

### Example Modifications

To add more histograms, define the histograms after the ones provided, for example:

```cpp
TH2D *h2_myHistogram = new TH2D("h2_myHistogram", "", 100, 0., 1., 100, 0., 1.);
h2_myHistogram->GetXaxis()->SetTitle("X-axis label");
h2_myHistogram->GetYaxis()->SetTitle("Y-axis label");
```

To apply user-defined selection criteria, add your selection code in the "APPLY YOUR SELECTIONS HERE" section, for example:

```cpp
if (Muons_PT_Leading < 50) continue; // Reject events with muon pT < 50 GeV
```

To perform additional calculations, add your calculations in the "PERFORM YOUR ANALYSIS HERE" section, for example:

```cpp
double myVariable = leadJet->Px() * subJet->Px();
```

To fill your histograms, add the Fill() function in the "FILL YOUR HISTOGRAMS HERE" section, for example:

```cpp
h2_myHistogram->Fill(myVariable, weight * constWeight);
```