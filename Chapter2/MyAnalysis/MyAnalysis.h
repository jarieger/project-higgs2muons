//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jun 16 10:13:28 2023 by ROOT version 6.24/06
// from TTree tree_Hmumu/output tree
//////////////////////////////////////////////////////////

#ifndef MyAnalysis_h
#define MyAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class MyAnalysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           run;
   ULong64_t       event;
   Int_t           Muons_Charge_Lead;
   Int_t           Muons_Charge_Sub;
   Int_t           Muons_Type_Lead;
   Int_t           Muons_Type_Sub;
   Float_t         Muons_Minv_MuMu;
   Float_t         Muons_PT_Lead;
   Float_t         Muons_PT_Sub;
   Float_t         Muons_Eta_Lead;
   Float_t         Muons_Eta_Sub;
   Float_t         Muons_Phi_Lead;
   Float_t         Muons_Phi_Sub;
   Float_t         Muons_Minv_MuMu_Fsr;
   Float_t         Muons_Minv_MuMu_Sigma;
   Float_t         Muons_Minv_MuMu_Fsr_Sigma;
   Float_t         Muons_DeltaEta_MuMu;
   Float_t         Muons_DeltaPhi_MuMu;
   Float_t         Muons_DeltaR_MuMu;
   Float_t         Muons_CosThetaStar;
   Float_t         FSR_Et;
   Float_t         FSR_Eta;
   Float_t         FSR_Phi;
   Float_t         FSR_f1;
   Float_t         FSR_DeltaR;
   Int_t           FSR_Type;
   Int_t           FSR_IsPhoton;
   Float_t         Z_PT;
   Float_t         Z_Y;
   Float_t         Z_Eta;
   Float_t         Z_Phi;
   Float_t         Z_PT_FSR;
   Float_t         Z_Y_FSR;
   Float_t         Z_Eta_FSR;
   Float_t         Z_Phi_FSR;
   Float_t         Event_MET;
   Float_t         Event_MET_Phi;
   Float_t         Event_MET_Sig;
   Float_t         Event_MET_Sig_CP;
   Float_t         Event_Ht;
   Int_t           Event_HasBJet;
   Float_t         Truth_PT_Lead_Muon;
   Float_t         Truth_PT_Sub_Muon;
   Float_t         Truth_Eta_Lead_Muon;
   Float_t         Truth_Eta_Sub_Muon;
   Float_t         Truth_Phi_Lead_Muon;
   Float_t         Truth_Phi_Sub_Muon;
   Float_t         Truth_Minv_MuMu;
   Float_t         TriggerSFWeight;
   Float_t         EventWeight_PileupWeight;
   Float_t         MuonSFWeight;
   Float_t         JetSFWeight;
   Float_t         TotalWeight;
   Float_t         EventWeight_MCEventWeight;
   Int_t           Jets_jetMultip;
   vector<float>   *Jets_PT;
   vector<float>   *Jets_Eta;
   vector<float>   *Jets_Phi;
   vector<float>   *Jets_E;
   vector<float>   *BtagSFWeight;
   Float_t         Jets_Minv_jj;
   Float_t         Jets_PT_jj;
   Float_t         Jets_PT_Lead;
   Float_t         Jets_PT_Sub;
   Float_t         Jets_Eta_Lead;
   Float_t         Jets_Eta_Sub;
   Float_t         Jets_Phi_Lead;
   Float_t         Jets_Phi_Sub;
   Float_t         Jets_E_Lead;
   Float_t         Jets_E_Sub;
   Int_t           Jets_PassFJVT_Lead;
   Int_t           Jets_PassFJVT_Sub;
   Float_t         Jets_DeltaR_jj;
   Float_t         Jets_DeltaEta_jj;
   Float_t         Jets_DeltaPhi_jj;
   Float_t         Jets_Eta_jj;
   Float_t         Jets_Phi_jj;
   Float_t         Jets_NTracks_Lead;
   Float_t         Jets_NTracks_Sub;
   Float_t         Event_Centrality;
   Float_t         Event_PT_MuMuj1;
   Float_t         Event_PT_MuMuj2;
   Float_t         Event_PT_MuMujj;
   Float_t         Event_Y_MuMuj1;
   Float_t         Event_Y_MuMuj2;
   Float_t         Event_Y_MuMujj;
   vector<int>     *Muons_Pos_PassTightIso;
   vector<int>     *Muons_Neg_PassTightIso;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_event;   //!
   TBranch        *b_Muons_Charge_Lead;   //!
   TBranch        *b_Muons_Charge_Sub;   //!
   TBranch        *b_Muons_Type_Lead;   //!
   TBranch        *b_Muons_Type_Sub;   //!
   TBranch        *b_Muons_Minv_MuMu;   //!
   TBranch        *b_Muons_PT_Lead;   //!
   TBranch        *b_Muons_PT_Sub;   //!
   TBranch        *b_Muons_Eta_Lead;   //!
   TBranch        *b_Muons_Eta_Sub;   //!
   TBranch        *b_Muons_Phi_Lead;   //!
   TBranch        *b_Muons_Phi_Sub;   //!
   TBranch        *b_Muons_Minv_MuMu_Fsr;   //!
   TBranch        *b_Muons_Minv_MuMu_Sigma;   //!
   TBranch        *b_Muons_Minv_MuMu_Fsr_Sigma;   //!
   TBranch        *b_Muons_DeltaEta_MuMu;   //!
   TBranch        *b_Muons_DeltaPhi_MuMu;   //!
   TBranch        *b_Muons_DeltaR_MuMu;   //!
   TBranch        *b_Muons_CosThetaStar;   //!
   TBranch        *b_FSR_Et;   //!
   TBranch        *b_FSR_Eta;   //!
   TBranch        *b_FSR_Phi;   //!
   TBranch        *b_FSR_f1;   //!
   TBranch        *b_FSR_DeltaR;   //!
   TBranch        *b_FSR_Type;   //!
   TBranch        *b_FSR_IsPhoton;   //!
   TBranch        *b_Z_PT;   //!
   TBranch        *b_Z_Y;   //!
   TBranch        *b_Z_Eta;   //!
   TBranch        *b_Z_Phi;   //!
   TBranch        *b_Z_PT_FSR;   //!
   TBranch        *b_Z_Y_FSR;   //!
   TBranch        *b_Z_Eta_FSR;   //!
   TBranch        *b_Z_Phi_FSR;   //!
   TBranch        *b_Event_MET;   //!
   TBranch        *b_Event_MET_Phi;   //!
   TBranch        *b_Event_MET_Sig;   //!
   TBranch        *b_Event_MET_Sig_CP;   //!
   TBranch        *b_Event_Ht;   //!
   TBranch        *b_Event_HasBJet;   //!
   TBranch        *b_Truth_PT_Lead_Muon;   //!
   TBranch        *b_Truth_PT_Sub_Muon;   //!
   TBranch        *b_Truth_Eta_Lead_Muon;   //!
   TBranch        *b_Truth_Eta_Sub_Muon;   //!
   TBranch        *b_Truth_Phi_Lead_Muon;   //!
   TBranch        *b_Truth_Phi_Sub_Muon;   //!
   TBranch        *b_Truth_Minv_MuMu;   //!
   TBranch        *b_TriggerSFWeight;   //!
   TBranch        *b_EventWeight_PileupWeight;   //!
   TBranch        *b_MuonSFWeight;   //!
   TBranch        *b_JetSFWeight;   //!
   TBranch        *b_TotalWeight;   //!
   TBranch        *b_EventWeight_MCEventWeight;   //!
   TBranch        *b_Jets_jetMultip;   //!
   TBranch        *b_Jets_PT;   //!
   TBranch        *b_Jets_Eta;   //!
   TBranch        *b_Jets_Phi;   //!
   TBranch        *b_Jets_E;   //!
   TBranch        *b_BtagSFWeight;   //!
   TBranch        *b_Jets_Minv_jj;   //!
   TBranch        *b_Jets_PT_jj;   //!
   TBranch        *b_Jets_PT_Lead;   //!
   TBranch        *b_Jets_PT_Sub;   //!
   TBranch        *b_Jets_Eta_Lead;   //!
   TBranch        *b_Jets_Eta_Sub;   //!
   TBranch        *b_Jets_Phi_Lead;   //!
   TBranch        *b_Jets_Phi_Sub;   //!
   TBranch        *b_Jets_E_Lead;   //!
   TBranch        *b_Jets_E_Sub;   //!
   TBranch        *b_Jets_PassFJVT_Lead;   //!
   TBranch        *b_Jets_PassFJVT_Sub;   //!
   TBranch        *b_Jets_DeltaR_jj;   //!
   TBranch        *b_Jets_DeltaEta_jj;   //!
   TBranch        *b_Jets_DeltaPhi_jj;   //!
   TBranch        *b_Jets_Eta_jj;   //!
   TBranch        *b_Jets_Phi_jj;   //!
   TBranch        *b_Jets_NTracks_Lead;   //!
   TBranch        *b_Jets_NTracks_Sub;   //!
   TBranch        *b_Event_Centrality;   //!
   TBranch        *b_Event_PT_MuMuj1;   //!
   TBranch        *b_Event_PT_MuMuj2;   //!
   TBranch        *b_Event_PT_MuMujj;   //!
   TBranch        *b_Event_Y_MuMuj1;   //!
   TBranch        *b_Event_Y_MuMuj2;   //!
   TBranch        *b_Event_Y_MuMujj;   //!
   TBranch        *b_Muons_Pos_PassTightIso;   //!
   TBranch        *b_Muons_Neg_PassTightIso;   //!

   MyAnalysis(TTree *tree=0);
   virtual ~MyAnalysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(int processID);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MyAnalysis_cxx
MyAnalysis::MyAnalysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../data/Run3/ggH_Run3.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../../data/Run3/ggH_Run3.root");
      }
      f->GetObject("tree_Hmumu",tree);

   }
   Init(tree);
}

MyAnalysis::~MyAnalysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MyAnalysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MyAnalysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MyAnalysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Jets_PT = 0;
   Jets_Eta = 0;
   Jets_Phi = 0;
   Jets_E = 0;
   BtagSFWeight = 0;
   Muons_Pos_PassTightIso = 0;
   Muons_Neg_PassTightIso = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("Muons_Charge_Lead", &Muons_Charge_Lead, &b_Muons_Charge_Lead);
   fChain->SetBranchAddress("Muons_Charge_Sub", &Muons_Charge_Sub, &b_Muons_Charge_Sub);
   fChain->SetBranchAddress("Muons_Type_Lead", &Muons_Type_Lead, &b_Muons_Type_Lead);
   fChain->SetBranchAddress("Muons_Type_Sub", &Muons_Type_Sub, &b_Muons_Type_Sub);
   fChain->SetBranchAddress("Muons_Minv_MuMu", &Muons_Minv_MuMu, &b_Muons_Minv_MuMu);
   fChain->SetBranchAddress("Muons_PT_Lead", &Muons_PT_Lead, &b_Muons_PT_Lead);
   fChain->SetBranchAddress("Muons_PT_Sub", &Muons_PT_Sub, &b_Muons_PT_Sub);
   fChain->SetBranchAddress("Muons_Eta_Lead", &Muons_Eta_Lead, &b_Muons_Eta_Lead);
   fChain->SetBranchAddress("Muons_Eta_Sub", &Muons_Eta_Sub, &b_Muons_Eta_Sub);
   fChain->SetBranchAddress("Muons_Phi_Lead", &Muons_Phi_Lead, &b_Muons_Phi_Lead);
   fChain->SetBranchAddress("Muons_Phi_Sub", &Muons_Phi_Sub, &b_Muons_Phi_Sub);
   fChain->SetBranchAddress("Muons_Minv_MuMu_Fsr", &Muons_Minv_MuMu_Fsr, &b_Muons_Minv_MuMu_Fsr);
   fChain->SetBranchAddress("Muons_Minv_MuMu_Sigma", &Muons_Minv_MuMu_Sigma, &b_Muons_Minv_MuMu_Sigma);
   fChain->SetBranchAddress("Muons_Minv_MuMu_Fsr_Sigma", &Muons_Minv_MuMu_Fsr_Sigma, &b_Muons_Minv_MuMu_Fsr_Sigma);
   fChain->SetBranchAddress("Muons_DeltaEta_MuMu", &Muons_DeltaEta_MuMu, &b_Muons_DeltaEta_MuMu);
   fChain->SetBranchAddress("Muons_DeltaPhi_MuMu", &Muons_DeltaPhi_MuMu, &b_Muons_DeltaPhi_MuMu);
   fChain->SetBranchAddress("Muons_DeltaR_MuMu", &Muons_DeltaR_MuMu, &b_Muons_DeltaR_MuMu);
   fChain->SetBranchAddress("Muons_CosThetaStar", &Muons_CosThetaStar, &b_Muons_CosThetaStar);
   fChain->SetBranchAddress("FSR_Et", &FSR_Et, &b_FSR_Et);
   fChain->SetBranchAddress("FSR_Eta", &FSR_Eta, &b_FSR_Eta);
   fChain->SetBranchAddress("FSR_Phi", &FSR_Phi, &b_FSR_Phi);
   fChain->SetBranchAddress("FSR_f1", &FSR_f1, &b_FSR_f1);
   fChain->SetBranchAddress("FSR_DeltaR", &FSR_DeltaR, &b_FSR_DeltaR);
   fChain->SetBranchAddress("FSR_Type", &FSR_Type, &b_FSR_Type);
   fChain->SetBranchAddress("FSR_IsPhoton", &FSR_IsPhoton, &b_FSR_IsPhoton);
   fChain->SetBranchAddress("Z_PT", &Z_PT, &b_Z_PT);
   fChain->SetBranchAddress("Z_Y", &Z_Y, &b_Z_Y);
   fChain->SetBranchAddress("Z_Eta", &Z_Eta, &b_Z_Eta);
   fChain->SetBranchAddress("Z_Phi", &Z_Phi, &b_Z_Phi);
   fChain->SetBranchAddress("Z_PT_FSR", &Z_PT_FSR, &b_Z_PT_FSR);
   fChain->SetBranchAddress("Z_Y_FSR", &Z_Y_FSR, &b_Z_Y_FSR);
   fChain->SetBranchAddress("Z_Eta_FSR", &Z_Eta_FSR, &b_Z_Eta_FSR);
   fChain->SetBranchAddress("Z_Phi_FSR", &Z_Phi_FSR, &b_Z_Phi_FSR);
   fChain->SetBranchAddress("Event_MET", &Event_MET, &b_Event_MET);
   fChain->SetBranchAddress("Event_MET_Phi", &Event_MET_Phi, &b_Event_MET_Phi);
   fChain->SetBranchAddress("Event_MET_Sig", &Event_MET_Sig, &b_Event_MET_Sig);
   fChain->SetBranchAddress("Event_MET_Sig_CP", &Event_MET_Sig_CP, &b_Event_MET_Sig_CP);
   fChain->SetBranchAddress("Event_Ht", &Event_Ht, &b_Event_Ht);
   fChain->SetBranchAddress("Event_HasBJet", &Event_HasBJet, &b_Event_HasBJet);
   fChain->SetBranchAddress("Truth_PT_Lead_Muon", &Truth_PT_Lead_Muon, &b_Truth_PT_Lead_Muon);
   fChain->SetBranchAddress("Truth_PT_Sub_Muon", &Truth_PT_Sub_Muon, &b_Truth_PT_Sub_Muon);
   fChain->SetBranchAddress("Truth_Eta_Lead_Muon", &Truth_Eta_Lead_Muon, &b_Truth_Eta_Lead_Muon);
   fChain->SetBranchAddress("Truth_Eta_Sub_Muon", &Truth_Eta_Sub_Muon, &b_Truth_Eta_Sub_Muon);
   fChain->SetBranchAddress("Truth_Phi_Lead_Muon", &Truth_Phi_Lead_Muon, &b_Truth_Phi_Lead_Muon);
   fChain->SetBranchAddress("Truth_Phi_Sub_Muon", &Truth_Phi_Sub_Muon, &b_Truth_Phi_Sub_Muon);
   fChain->SetBranchAddress("Truth_Minv_MuMu", &Truth_Minv_MuMu, &b_Truth_Minv_MuMu);
   fChain->SetBranchAddress("TriggerSFWeight", &TriggerSFWeight, &b_TriggerSFWeight);
   fChain->SetBranchAddress("EventWeight_PileupWeight", &EventWeight_PileupWeight, &b_EventWeight_PileupWeight);
   fChain->SetBranchAddress("MuonSFWeight", &MuonSFWeight, &b_MuonSFWeight);
   fChain->SetBranchAddress("JetSFWeight", &JetSFWeight, &b_JetSFWeight);
   fChain->SetBranchAddress("TotalWeight", &TotalWeight, &b_TotalWeight);
   fChain->SetBranchAddress("EventWeight_MCEventWeight", &EventWeight_MCEventWeight, &b_EventWeight_MCEventWeight);
   fChain->SetBranchAddress("Jets_jetMultip", &Jets_jetMultip, &b_Jets_jetMultip);
   fChain->SetBranchAddress("Jets_PT", &Jets_PT, &b_Jets_PT);
   fChain->SetBranchAddress("Jets_Eta", &Jets_Eta, &b_Jets_Eta);
   fChain->SetBranchAddress("Jets_Phi", &Jets_Phi, &b_Jets_Phi);
   fChain->SetBranchAddress("Jets_E", &Jets_E, &b_Jets_E);
   fChain->SetBranchAddress("BtagSFWeight", &BtagSFWeight, &b_BtagSFWeight);
   fChain->SetBranchAddress("Jets_Minv_jj", &Jets_Minv_jj, &b_Jets_Minv_jj);
   fChain->SetBranchAddress("Jets_PT_jj", &Jets_PT_jj, &b_Jets_PT_jj);
   fChain->SetBranchAddress("Jets_PT_Lead", &Jets_PT_Lead, &b_Jets_PT_Lead);
   fChain->SetBranchAddress("Jets_PT_Sub", &Jets_PT_Sub, &b_Jets_PT_Sub);
   fChain->SetBranchAddress("Jets_Eta_Lead", &Jets_Eta_Lead, &b_Jets_Eta_Lead);
   fChain->SetBranchAddress("Jets_Eta_Sub", &Jets_Eta_Sub, &b_Jets_Eta_Sub);
   fChain->SetBranchAddress("Jets_Phi_Lead", &Jets_Phi_Lead, &b_Jets_Phi_Lead);
   fChain->SetBranchAddress("Jets_Phi_Sub", &Jets_Phi_Sub, &b_Jets_Phi_Sub);
   fChain->SetBranchAddress("Jets_E_Lead", &Jets_E_Lead, &b_Jets_E_Lead);
   fChain->SetBranchAddress("Jets_E_Sub", &Jets_E_Sub, &b_Jets_E_Sub);
   fChain->SetBranchAddress("Jets_PassFJVT_Lead", &Jets_PassFJVT_Lead, &b_Jets_PassFJVT_Lead);
   fChain->SetBranchAddress("Jets_PassFJVT_Sub", &Jets_PassFJVT_Sub, &b_Jets_PassFJVT_Sub);
   fChain->SetBranchAddress("Jets_DeltaR_jj", &Jets_DeltaR_jj, &b_Jets_DeltaR_jj);
   fChain->SetBranchAddress("Jets_DeltaEta_jj", &Jets_DeltaEta_jj, &b_Jets_DeltaEta_jj);
   fChain->SetBranchAddress("Jets_DeltaPhi_jj", &Jets_DeltaPhi_jj, &b_Jets_DeltaPhi_jj);
   fChain->SetBranchAddress("Jets_Eta_jj", &Jets_Eta_jj, &b_Jets_Eta_jj);
   fChain->SetBranchAddress("Jets_Phi_jj", &Jets_Phi_jj, &b_Jets_Phi_jj);
   fChain->SetBranchAddress("Jets_NTracks_Lead", &Jets_NTracks_Lead, &b_Jets_NTracks_Lead);
   fChain->SetBranchAddress("Jets_NTracks_Sub", &Jets_NTracks_Sub, &b_Jets_NTracks_Sub);
   fChain->SetBranchAddress("Event_Centrality", &Event_Centrality, &b_Event_Centrality);
   fChain->SetBranchAddress("Event_PT_MuMuj1", &Event_PT_MuMuj1, &b_Event_PT_MuMuj1);
   fChain->SetBranchAddress("Event_PT_MuMuj2", &Event_PT_MuMuj2, &b_Event_PT_MuMuj2);
   fChain->SetBranchAddress("Event_PT_MuMujj", &Event_PT_MuMujj, &b_Event_PT_MuMujj);
   fChain->SetBranchAddress("Event_Y_MuMuj1", &Event_Y_MuMuj1, &b_Event_Y_MuMuj1);
   fChain->SetBranchAddress("Event_Y_MuMuj2", &Event_Y_MuMuj2, &b_Event_Y_MuMuj2);
   fChain->SetBranchAddress("Event_Y_MuMujj", &Event_Y_MuMujj, &b_Event_Y_MuMujj);
   fChain->SetBranchAddress("Muons_Pos_PassTightIso", &Muons_Pos_PassTightIso, &b_Muons_Pos_PassTightIso);
   fChain->SetBranchAddress("Muons_Neg_PassTightIso", &Muons_Neg_PassTightIso, &b_Muons_Neg_PassTightIso);
   Notify();
}

Bool_t MyAnalysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MyAnalysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MyAnalysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MyAnalysis_cxx
