# EasyCompare

## Overview

The EasyCompare function is a simple and useful script for comparing distributions between a signal and a chosen background process. It evaluates the efficiency, rejection, and significance of the selection applied to the input data.

## Function Parameters

- **bkgName** (default: "DrellYan"): The name of the background process. Supported options: "DrellYan", "Top", "Diboson"
- **varName** (default: Muons_Minv_MuMu): The variable to be used for the comparison.
- **binning** (default: 50): The number of bins for the histograms.
- **xmin** (default: 110): The lower limit of the x-axis.
- **xmax** (default: 160): The upper limit of the x-axis.
- **logY** (default: true): Set the y-axis to logarithmic scale if true.
- **norm**: True/False to normalize the histograms or not (to compare the shapes of the distributions).
- **selection** (default: ""): The selection applied to the input data.
- **selectionName** (default: selectedEvents): The name of the selection.
- **Lumi** (default: 56.): Luminosity.

## How to Run

First, compile the script using ROOT:

```bash
root 
.L EasyCompare.C
```
Then, run the script by calling the EasyCompare function in a ROOT interactive session:
```
EasyCompare("DrellYan", "Muons_Minv_MuMu", 50, 110, 160, true, false, "max(abs(Muons_Eta_Lead),abs(Muons_Eta_Sub))<1.0", "centralMuons");
EasyCompare("Top", "Muons_Minv_MuMu", 50, 110, 160, false, true, "Muons_PT_Lead>50", "highPT");
EasyCompare("Diboson", "Muons_Minv_MuMu", 50, 110, 160, false, false, "abs(Jets_Eta_Lead)>3.0", "forwardJets");
```

The examples above will compare the signal process (VBF) with the background processes (DrellYan, Top, and Diboson) with the given selection and variable. The histograms will be plotted and saved as PDF files.