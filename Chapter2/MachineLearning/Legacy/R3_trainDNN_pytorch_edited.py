import ROOT
from ROOT import TMVA, TFile, TTree, TCut
from subprocess import call
from os.path import isfile
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import ctypes
import torch
from torch import nn

# cpp_includes = """
# #include <iostream>
# #include <vector>
# #include <string>

# #include "TH1F.h"
# #include "TPad.h"
# #include "TRandom.h"
# #include "TCanvas.h"
# #include "TFile.h"
# #include "TTree.h"
# #include "TLatex.h"
# #include "TCut.h"
# #include "TLegend.h"
# #include "TString.h"
# #include "TDirectory.h"
# """
# ROOT.gInterpreter.ProcessLine(cpp_includes)

# from R3_ROCMaker import R3_ROCMaker

# ROOT.gInterpreter.ProcessLine('#include "R3_BgDistrPlot.h"')
# ROOT.gInterpreter.ProcessLine('#include "R3_Fairness2PlotNNMovingWindow.h"')
# ROOT.gInterpreter.ProcessLine('#include "R3_Fairness2PlotNNQuantilesUnstacked.h"')

global load_model_custom_objects

def main():
    # Observables 
    OBSERVABLES = ["Muons_PT_Lead","Muons_PT_Sub", "Muons_Eta_Lead", "Muons_Eta_Sub", "Muons_Phi_Lead", "Muons_Phi_Sub", "Muons_PT_Lead/Muons_Minv_MuMu","Muons_PT_Sub/Muons_Minv_MuMu"]

    # List of runs to train
    # (str RUNNAME, list<str> input observables, str selection, bool weighted, int binMin, int binMax)
    RUNS = [
        # ("", OBSERVABLES[:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100, 180),
        # ("_no_PT_Lead",OBSERVABLES[1:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_no_PT_Sub", OBSERVABLES[:1] + OBSERVABLES[2:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_no_Eta_Lead", OBSERVABLES[:2] + OBSERVABLES[3:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_no_Eta_Sub", OBSERVABLES[:3] + OBSERVABLES[4:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_no_Phi_Lead", OBSERVABLES[:4] + OBSERVABLES[5:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_no_Phi_Sub", OBSERVABLES[:5], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_Minv_over_PT", OBSERVABLES[2:], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_Minv_over_PT_no_Eta_Lead", OBSERVABLES[3:], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_Minv_over_PT_no_Eta_Sub", OBSERVABLES[2:3] + OBSERVABLES[4:] , "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_Minv_over_PT_no_Phi_Lead", OBSERVABLES[2:4] + OBSERVABLES[5:], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_Minv_over_PT_no_Phi_Sub", OBSERVABLES[2:5] + OBSERVABLES[6:], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", False, 100 ,180),
        ("_Reweight", OBSERVABLES[:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 140", True, 100 ,140),
        ("_Reweight_Uncapped", OBSERVABLES[:6], "Muons_Minv_MuMu >= 100 && Muons_Minv_MuMu <= 180", True, 100 ,180)
    ]

    for runName, obs, trWind, w, m, M in RUNS:

        runName = "_modRR" + runName

        print(f"TMVA_R3{runName}: using {obs}")
        # print("magic1: " , load_model_custom_objects)
        R3_TrainDNN(runName, obs, trWind, w)
        # print("magic2: " , load_model_custom_objects)

        print("-- trained succesfully")
        # R3_ROCMaker("TMVA_R3" + runName)
        # print("-- made ROC")
        # ROOT.R3_BgDistrPlot("TMVA_R3" + runName)
        # print("-- made distr plot")
        # ROOT.R3_Fairness2PlotNNQuantilesUnstacked("TMVA_R3" + runName)
        # print("-- made FQU")
        # ROOT.R3_Fairness2PlotNNMovingWindow("TMVA_R3" + runName)
        # print("-- made FMW")

        print(f"\n\nTMVA_R3{runName} done! \n\n")


    return 0

def R3_TrainDNN( runName="", observables=[],trainSelection="Muons_Minv_MuMu >= 110 && Muons_Minv_MuMu <= 160", weighted=False):
    global load_model_custom_objects
    # print("training: ", load_model_custom_objects)

    # Suffix to append to all saved files for simple ordering
    # Edit as desired for each run
    RUNNAME = "_R3" + runName


    # Setup TMVA
    TMVA.Tools.Instance()
    TMVA.PyMethodBase.PyInitialize()
    
    output = TFile.Open('TMVA'+RUNNAME+'.root', 'RECREATE')
    if not output or  output.IsZombie(): del output; return
    # factory = TMVA.Factory('TMVAClassification', output,
    #                        '!V:!Silent:Color:DrawProgressBar:Transformations=D,G:AnalysisType=Classification')
    factory = TMVA.Factory("TMVA_CNN_Classification", output,
        "!V:ROC:!Silent:Color:AnalysisType=Classification:Transformations=None:!Correlations")


    if observables == []:
        observables = ["Muons_PT_Lead","Muons_PT_Sub", "Muons_Eta_Lead", "Muons_Eta_Sub", "Muons_Phi_Lead", "Muons_Phi_Sub"]
    # Storage: ["Muons_PT_Lead/Muons_Minv_MuMu","Muons_PT_Sub/Muons_Minv_MuMu", "Muons_Eta_Lead", "Muons_Eta_Sub", "Muons_Phi_Lead", "Muons_Phi_Sub"]


    dataloader = TMVA.DataLoader('dataset')
    for entry in observables:
        print(entry)
        dataloader.AddVariable(entry)

    dataloader.AddSpectator("Muons_Minv_MuMu")
    if weighted:
        dataloader.SetWeightExpression("EventWeight_FineMinvReweight")
    

    #Load data 
    signal_data = TFile.Open('/project/atlas/users/jegbers/project-higgs2muons/Run3/input_samples_reweighted/ggH_Run3.root')
    signal = signal_data.Get('tree_Hmumu')
    dataloader.AddSignalTree(signal, 1.0)


    background_data = TFile.Open('/project/atlas/users/jegbers/project-higgs2muons/Run3/input_samples_reweighted/Zmumu_Run3.root')
    background = background_data.Get('tree_Hmumu')
    dataloader.AddBackgroundTree(background, 1.0)


    dataloader.PrepareTrainingAndTestTree(TCut(trainSelection), 'nTrain_Signal=200000:nTrain_Background=200000::nTest_Signal=150000:nTest_Background=1000000:SplitMode=Random:NormMode=NumEvents:!V')
    # storage: Muons_Minv_MuMu >= 110 && Muons_Minv_MuMu <= 140
    
    # Generate model
    
    # # # Define feed-forward models
    # # This is the model used so far
    # mNAME = "ModelClassification"
    # model = nn.Sequential()
    # model.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model.add_module('bn1', nn.BatchNorm1d(64)) # Add BatchNorm Layer
    # model.add_module('relu', nn.ReLU())
    # model.add_module('linear_2', nn.Linear(in_features=64, out_features=64))
    # model.add_module('linear_3', nn.Linear(in_features=64, out_features=32))
    # model.add_module('linear_4', nn.Linear(in_features=32, out_features=2))
    # model.add_module('softmax', nn.Softmax(dim=1))

    # # # Define feed-forward model
    # m2NAME = "modelLargeClassification"
    # model2 = nn.Sequential()
    # model2.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=128))
    # model2.add_module('bn1', nn.BatchNorm1d(128)) # Add BatchNorm Layer
    # model2.add_module('relu', nn.ReLU())
    # model2.add_module('linear_2', nn.Linear(in_features=128, out_features=64))
    # model2.add_module('linear_3', nn.Linear(in_features=64, out_features=32))
    # model2.add_module('linear_4', nn.Linear(in_features=32, out_features=2))
    # model2.add_module('softmax', nn.Softmax(dim=1))


    # # # Define feed-forward model
    # m3NAME = "modelShallowClassification"
    # model3 = nn.Sequential()
    # model3.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model3.add_module('bn1', nn.BatchNorm1d(64)) # Add BatchNorm Layer
    # model3.add_module('relu', nn.ReLU())
    # model3.add_module('linear_2', nn.Linear(in_features=64, out_features=32))
    # model3.add_module('linear_3', nn.Linear(in_features=32, out_features=2))
    # model3.add_module('softmax', nn.Softmax(dim=1))


    # # # Define feed-forward model
    # m4NAME = "modelDeepClassification"
    # model4 = nn.Sequential()
    # model4.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model4.add_module('bn1', nn.BatchNorm1d(64)) # Add BatchNorm Layer
    # model4.add_module('relu', nn.ReLU())
    # model2.add_module('linear_2', nn.Linear(in_features=128, out_features=64))
    # model4.add_module('linear_3', nn.Linear(in_features=64, out_features=64))
    # model4.add_module('linear_4', nn.Linear(in_features=64, out_features=64))
    # model4.add_module('linear_5', nn.Linear(in_features=64, out_features=32))
    # model4.add_module('linear_6', nn.Linear(in_features=32, out_features=2))
    # model4.add_module('softmax', nn.Softmax(dim=1))

    # !!! Best performer of these 6 !!!
    # # # Define feed-forward model
    # m5NAME = "modelReapliedReLUClassification"
    # model5 = nn.Sequential()
    # model5.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model5.add_module('bn1', nn.BatchNorm1d(64)) # Add BatchNorm Layer
    # model5.add_module('relu', nn.ReLU())
    # model5.add_module('linear_2', nn.Linear(in_features=64, out_features=64))
    # model5.add_module('relu2', nn.ReLU())
    # model5.add_module('linear_3', nn.Linear(in_features=64, out_features=32))
    # model5.add_module('relu3', nn.ReLU())
    # model5.add_module('linear_4', nn.Linear(in_features=32, out_features=2))
    # model5.add_module('softmax', nn.Softmax(dim=1))

    # # # Define feed-forward models
    # m6NAME = "modelSigmoidClassification"
    # model6 = nn.Sequential()
    # model6.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model6.add_module('bn1', nn.BatchNorm1d(64)) # Add BatchNorm Layer
    # model6.add_module('sigm', nn.Sigmoid())
    # model6.add_module('linear_2', nn.Linear(in_features=64, out_features=64))
    # model6.add_module('linear_3', nn.Linear(in_features=64, out_features=32))
    # model6.add_module('linear_4', nn.Linear(in_features=32, out_features=2))
    # model6.add_module('softmax', nn.Softmax(dim=1))

        
    # # # Define feed-forward models
    # # This is the model used so far
    # mNAME = "model_RR_Base"
    # model = nn.Sequential()
    # model.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model.add_module('bn1', nn.BatchNorm1d(64)) # Add BatchNorm Layer
    # model.add_module('relu', nn.ReLU())
    # model.add_module('linear_2', nn.Linear(in_features=64, out_features=64))
    # model.add_module('relu2', nn.ReLU())
    # model.add_module('linear_3', nn.Linear(in_features=64, out_features=32))
    # model.add_module('relu3', nn.ReLU())
    # model.add_module('linear_4', nn.Linear(in_features=32, out_features=2))
    # model.add_module('softmax', nn.Softmax(dim=1))

    # # Define feed-forward model
    m2NAME = "model_RR_DeepWide"
    model2 = nn.Sequential()
    model2.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=128))
    model2.add_module('bn1', nn.BatchNorm1d(128)) # Add BatchNorm Layer
    model2.add_module('relu1', nn.ReLU())
    model2.add_module('linear_2', nn.Linear(in_features=128, out_features=64))
    model2.add_module('relu2', nn.ReLU())
    model2.add_module('linear_3', nn.Linear(in_features=64, out_features=64))
    model2.add_module('relu3', nn.ReLU())
    model2.add_module('linear_4', nn.Linear(in_features=64, out_features=32))
    model2.add_module('relu4', nn.ReLU())
    model2.add_module('linear_5', nn.Linear(in_features=32, out_features=2))
    model2.add_module('softmax', nn.Softmax(dim=1))


    # # # Define feed-forward model
    # m3NAME = "model_RR_SuperdeepWide"
    # model3 = nn.Sequential()
    # model3.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=128))
    # model3.add_module('bn1', nn.BatchNorm1d(128)) # Add BatchNorm Layer
    # model3.add_module('relu', nn.ReLU())
    # model3.add_module('linear_2', nn.Linear(in_features=128, out_features=128))
    # model3.add_module('relu2', nn.ReLU())
    # model3.add_module('linear_3', nn.Linear(in_features=128, out_features=64))
    # model3.add_module('relu3', nn.ReLU())
    # model3.add_module('linear_4', nn.Linear(in_features=64, out_features=64))
    # model3.add_module('relu4', nn.ReLU())
    # model3.add_module('linear_5', nn.Linear(in_features=64, out_features=32))
    # model3.add_module('relu5', nn.ReLU())
    # model3.add_module('linear_6', nn.Linear(in_features=32, out_features=8))
    # model3.add_module('relu6', nn.ReLU())
    # model3.add_module('linear_7', nn.Linear(in_features=8, out_features=2))
    # model3.add_module('softmax', nn.Softmax(dim=1))


    # # # Define feed-forward model
    # m4NAME = "model_RR_DeepSuperwide"
    # model4 = nn.Sequential()
    # model4.add_module('linear_1', nn.Linear(in_features=len(observables), out_features=64))
    # model4.add_module('bn1', nn.BatchNorm1d(512)) # Add BatchNorm Layer
    # model4.add_module('relu', nn.ReLU())
    # model4.add_module('linear_2', nn.Linear(in_features=512, out_features=256))
    # model4.add_module('relu2', nn.ReLU())
    # model4.add_module('linear_3', nn.Linear(in_features=256, out_features=128))
    # model4.add_module('relu3', nn.ReLU())
    # model4.add_module('linear_4', nn.Linear(in_features=128, out_features=128))
    # model4.add_module('relu4', nn.ReLU())
    # model4.add_module('linear_5', nn.Linear(in_features=128, out_features=32))
    # model4.add_module('relu5', nn.ReLU())
    # model4.add_module('linear_6', nn.Linear(in_features=32, out_features=2))
    # model4.add_module('softmax', nn.Softmax(dim=1))



    # Construct loss function and Optimizer.
    loss = torch.nn.MSELoss()
    optimizer = torch.optim.SGD
    
    
    # Define train function
    def train(model, train_loader, val_loader, num_epochs, batch_size, optimizer, criterion, save_best, scheduler):
        trainer = optimizer(model.parameters(), lr=0.01)
        schedule, schedulerSteps = scheduler
        best_val = None
    
        for epoch in range(num_epochs):
            # Training Loop
            # Set to train mode
            model.train()
            # running_train_loss = 0.0
            running_val_loss = 0.0
            for i, (X, y) in enumerate(train_loader):
                trainer.zero_grad()
                output = model(X)
                train_loss = criterion(output, y)
                train_loss.backward()
                trainer.step()
    
                # print train statistics
                # running_train_loss += train_loss.item()
                # if i % 32 == 31:    # print every 32 mini-batches
                #     # print("[{}, {}] train loss: {:.3f}".format(epoch+1, i+1, running_train_loss / 32))
                #     running_train_loss = 0.0
    
            if schedule:
                schedule(optimizer, epoch, schedulerSteps)
    
            # Validation Loop
            # Set to eval mode
            model.eval()
            with torch.no_grad():
                for i, (X, y) in enumerate(val_loader):
                    output = model(X)
                    val_loss = criterion(output, y)
                    running_val_loss += val_loss.item()
    
                curr_val = running_val_loss / len(val_loader)
                if save_best:
                    if best_val==None:
                        best_val = curr_val
                    best_val = save_best(model, curr_val, best_val)
    
                # print val statistics per epoch
                print("[{}] val loss: {:.3f}".format(epoch+1, curr_val))
                running_val_loss = 0.0
    
        print("Finished Training on {} Epochs!".format(epoch+1))
    
        return model
    
    
    # Define predict function
    def predict(model, test_X, batch_size=32):
        # Set to eval mode
        model.eval()
    
        test_dataset = torch.utils.data.TensorDataset(torch.Tensor(test_X))
        test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=False)
    
        predictions = []
        with torch.no_grad():
            for i, data in enumerate(test_loader):
                X = data[0]
                outputs = model(X)
                predictions.append(outputs)
            preds = torch.cat(predictions)
    
        return preds.numpy()
    
    load_model_custom_objects = {"optimizer": optimizer, "criterion": loss, "train_func": train, "predict_func": predict}
    
    # Store model to file
    # Convert the model to torchscript before saving
    # m = torch.jit.script(model)
    # torch.jit.save(m, mNAME+RUNNAME+".pt")
    # torch.jit.save(m, mNAME+RUNNAME+"10_128.pt")
    # # torch.jit.save(m, mNAME+RUNNAME+"200_128.pt")
    # torch.jit.save(m, mNAME+RUNNAME+"10_512.pt")
    # # torch.jit.save(m, mNAME+RUNNAME+"200_512.pt")
    # torch.jit.save(m, mNAME+RUNNAME+"10_1024.pt")

    # print(m)

    # Convert the model to torchscript before saving
    m2 = torch.jit.script(model2)
    torch.jit.save(m2, m2NAME+RUNNAME+".pt")
    # print(m2)
    
    # # Convert the model to torchscript before saving
    # m3 = torch.jit.script(model3)
    # torch.jit.save(m3, m3NAME+RUNNAME+".pt")
    # # print(m3)

    # # Convert the model to torchscript before saving
    # m4 = torch.jit.script(model4)
    # torch.jit.save(m4, m4NAME+RUNNAME+".pt")
    # # print(m4)
    
    # # Convert the model to torchscript before saving
    # m5 = torch.jit.script(model5)
    # torch.jit.save(m5, m5NAME+RUNNAME+".pt")
    # # print(m5)
    
    # # Convert the model to torchscript before saving
    # m6 = torch.jit.script(model6)
    # torch.jit.save(m6, m6NAME+RUNNAME+".pt")
    # # print(m6)
    
    
    
    
    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorch10_1024',
    #                 'H:!V:VarTransform=D,G:FilenameModel='+mNAME+RUNNAME+'10_1024.pt:FilenameTrainedModel=trained'+mNAME+RUNNAME+'10_1024.pt:NumEpochs=10:BatchSize=1024')

    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorch10_512',
    #                 'H:!V:VarTransform=D,G:FilenameModel='+mNAME+RUNNAME+'10_512.pt:FilenameTrainedModel=trained'+mNAME+RUNNAME+'10_512.pt:NumEpochs=10:BatchSize=512')
    
    # # # Book methods
    # # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorch200_512',
    # #                 'H:!V:VarTransform=D,G:FilenameModel='+mNAME+RUNNAME+'200_512.pt:FilenameTrainedModel=trained'+mNAME+RUNNAME+'200_512.pt:NumEpochs=200:BatchSize=512')
    
    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorch10_128',
    #                 'H:!V:VarTransform=D,G:FilenameModel='+mNAME+RUNNAME+'10_128.pt:FilenameTrainedModel=trained'+mNAME+RUNNAME+'10_128.pt:NumEpochs=10:BatchSize=128')
    
    # # # Book methods
    # # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorch200_128',
    # #                 'H:!V:VarTransform=D,G:FilenameModel='+mNAME+RUNNAME+'200_128.pt:FilenameTrainedModel=trained'+mNAME+RUNNAME+'200_128.pt:NumEpochs=200:BatchSize=128')
    
    # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorchRR',
    #                 'H:!V:VarTransform=D,G:FilenameModel='+mNAME+RUNNAME+'.pt:FilenameTrainedModel=trained'+mNAME+RUNNAME+'.pt:NumEpochs=80:BatchSize=32')

    # Book methods
    factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorch',
                       'H:!V:VarTransform=D,G:FilenameModel='+m2NAME+RUNNAME+'.pt:FilenameTrainedModel=trained'+m2NAME+RUNNAME+'.pt:NumEpochs=100:BatchSize=64')

    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorchRR_SD_W',
    #                    'H:!V:VarTransform=D,G:FilenameModel='+m3NAME+RUNNAME+'.pt:FilenameTrainedModel=trained'+m3NAME+RUNNAME+'.pt:NumEpochs=80:BatchSize=32')

    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorchRR_D_SW',
    #                    'H:!V:VarTransform=D,G:FilenameModel='+m4NAME+RUNNAME+'.pt:FilenameTrainedModel=trained'+m4NAME+RUNNAME+'.pt:NumEpochs=80:BatchSize=32')

    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorchReappliedReLU',
    #                    'H:!V:VarTransform=D,G:FilenameModel='+m5NAME+RUNNAME+'.pt:FilenameTrainedModel=trained'+m5NAME+RUNNAME+'.pt:NumEpochs=40:BatchSize=32')

    # # Book methods
    # factory.BookMethod(dataloader, TMVA.Types.kPyTorch, 'PyTorchSigmoid',
    #                    'H:!V:VarTransform=D,G:FilenameModel='+m6NAME+RUNNAME+'.pt:FilenameTrainedModel=trained'+m6NAME+RUNNAME+'.pt:NumEpochs=40:BatchSize=32')

    

    
    # Run training, test and evaluation
    factory.TrainAllMethods()
    factory.TestAllMethods()
    factory.EvaluateAllMethods()
    
    
    # Plot ROC Curves
    roc = factory.GetROCCurve(dataloader)
    roc.SaveAs('ROC_ClassificationPyTorch'+RUNNAME+'.pdf')

    output.Write()
    output.Close()
    del output


if __name__=="__main__":
    # R3_TrainDNN("_MultiModelRR_80_32")
    main()