#include "TString.h"

const int nVariables = 2;

TString VariableName[nVariables] = {"Muons_PT_Lead",
				                    "Muons_PT_Sub",
};

char VariableType[nVariables] = {'F',  //1
				                 'F',  //2                                                                                                                                     
};  

const int nSpectator = 1;
TString SpectatorName[nSpectator] = {"Muons_Minv_MuMu"};
                                     
char SpectatorType[nSpectator] = {'F'};
