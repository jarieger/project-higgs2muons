# Chapter 2. 

This Chapter is about optimising the event selection. By carefully choosing the criteria for the event selection, one can increase the number of signal events relative to background events, resulting in a higher signal-to-background ratio. This can improve the sensitivity of the analysis to detect rare or new physics phenomena. The chapter contains the following folders:

- **histos/**: This folder contains the histograms generated from the data analysis. The histograms are saved in PDF/PNG format and can be used for further analysis or plotting.
- **MachineLearning/**: This folder contains the code used for the machine learning analysis. The script can be used to train a boosted decidion tree to distingisuh signal and background events.
- **MyAnalysis/**: This folder contains the code for the main data analysis. It allows to apply an event selection, and creation of new kinematic observables based in the four-momenta of the muons and jets.
- **PlotMaker/**: This folder contains the code for making plots. The PlotMaker2 script will plot all the histograms generated with the MyAnalysis script.
- **plots/**: This folder contains the plots generated from the PlotMaker code, and the EasyCompare macro. 
- **ROOTmacros/**: This folder contains the EasyCompare maco to compare the signal and background distributions, calcualte the significance of the selection, and the efficiency and rejection of the selection.