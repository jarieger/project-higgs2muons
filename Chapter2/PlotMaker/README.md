# PlotMaker

This macro is used to automate the process of plotting histograms and 2D histograms from ROOT files in a consistent and customizable way.

The `PlotMaker2()` function processes signal and background ROOT files, extracts the histograms and 2D maps, and generates plots in the `plots/` directory. The function has a default option to display the Y-axis in a logarithmic scale (logY=true).

To run the macro, simply load it into a ROOT session and call the `PlotMaker2()` function.

```cpp
root PlotMaker2.C
```
or do run it interactivly (arguemnt false is to not use log for Y axis):

```cpp
root
.L PlotMaker2.C
PlotMaker2(false)
```