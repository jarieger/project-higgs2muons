#include <TMath.h>

#include "../../style/AtlasLabels.h"
#include "../../style/AtlasLabels.C"
#include "../../style/AtlasStyle.h"

#ifdef __CINT__
#include "../../style/AtlasStyle.C" 
#endif

void InitializeROOT() {
   gROOT->Reset();
   gROOT->SetBatch(kTRUE);
   gSystem->Load("libMathCore");
}

std::vector<TFile*> open_files(std::vector<std::string> filenames) {
    std::vector<TFile*> files;
    for (const auto& filename : filenames) {
        TFile* file = TFile::Open(filename.c_str(), "READ");
        if (!file || file->IsZombie()) {
            std::cerr << "Error: Cannot open file " << filename << std::endl;
            continue;
        }
        files.push_back(file);
    }
    return files;
}

std::map<std::string, TH1F*> get_histograms(TFile* file) {
    std::map<std::string, TH1F*> histograms;
    TKey *key;
    TIter nextkey(file->GetListOfKeys());
    while ((key = (TKey*)nextkey())) {
        TObject *obj = key->ReadObj();
        if (obj->IsA()->InheritsFrom("TH1") && !obj->IsA()->InheritsFrom("TH2")) {
            TH1F* histogram = (TH1F*)obj;
            histograms[histogram->GetName()] = histogram;
        }
    }
    return histograms;
}

std::map<std::string, TH2F*> get_maps(TFile* file) {
    std::map<std::string, TH2F*> histograms;
    TKey *key;
    TIter nextkey(file->GetListOfKeys());
    while ((key = (TKey*)nextkey())) {
        TObject *obj = key->ReadObj();
        if (obj->IsA()->InheritsFrom("TH2")) {
            TH2F* histogram = (TH2F*)obj;
            histograms[histogram->GetName()] = histogram;
        }
    }
    return histograms;
}

std::string extractSubstring(const std::string &inputStr)
{
    std::string prefixToRemove = "../histos/";
    std::string targetToRemove = "output.root";
    std::string result = inputStr.substr(prefixToRemove.length());
    size_t targetPos = result.find(targetToRemove);
    result.erase(targetPos, targetToRemove.length());

    return result;
}

std::string parse_process_name(const std::string& unique_name) {
    // Cut out the part before "_Run3"
    size_t pos = unique_name.find("_Run3");
    if (pos != std::string::npos) {
        return unique_name.substr(0, pos);
    } else {
        return unique_name;
    }
}

std::map<std::string, TH2F*> collect_maps(const std::vector<TFile*>& files) {
    std::map<std::string, TH2F*> all_maps;

    for (auto file : files) {
        auto maps = get_maps(file);
        for (auto map : maps) {
            std::string unique_name = file->GetName();
            unique_name = extractSubstring(unique_name);
            unique_name += map.first;
            // std::cout << unique_name << std::endl;
            all_maps[unique_name] = map.second;
        }
    }
    return all_maps;
}

// This function sorts a vector of pairs of TH1F* and string by the Integral() of the TH1F*.
std::vector<std::pair<TH1F*, std::string>> sort_by_integral(const std::vector<std::pair<TH1F*, std::string>>& hist_vec) {
    std::vector<std::pair<TH1F*, std::string>> sorted_vec(hist_vec);
    std::sort(sorted_vec.begin(), sorted_vec.end(), [](const std::pair<TH1F*, std::string>& a, const std::pair<TH1F*, std::string>& b) {
        return a.first->Integral() > b.first->Integral();
    });
    return sorted_vec;
}

// This function adds the histograms in a vector of pairs of TH1F* and string in reverse order.
void add_in_reverse_order(std::vector<std::pair<TH1F*, std::string>>& hist_vec) {
    for (int i = hist_vec.size() - 2; i >= 0; --i) {
        hist_vec[i].first->Add(hist_vec[i+1].first);
    }
}

std::map<std::string, std::vector<std::pair<TH1F*, std::string>>> collect_histos(const std::vector<TFile*>& files) {
    std::map<std::string, std::vector<std::pair<TH1F*, std::string>>> all_histos;

    for (auto file : files) {
        auto histos = get_histograms(file);
        for (auto hist : histos) {
            std::string full_name = file->GetName();
            full_name = extractSubstring(full_name);
            full_name += hist.first;

            std::string hist_name = hist.first;

            // Check if this histogram already exists in the map
            if (all_histos.find(hist_name) == all_histos.end()) {
                // If it doesn't exist, create a new vector
                all_histos[hist_name] = std::vector<std::pair<TH1F*, std::string>>();
            }

            // Add the histogram to the corresponding vector in the map
            all_histos[hist_name].push_back(std::make_pair(hist.second, full_name));
        }
    }

    // Now, we sort the vectors by integral and add in reverse order
    for (auto& entry : all_histos) {
        auto& hist_vec = entry.second;
        hist_vec = sort_by_integral(hist_vec);
        add_in_reverse_order(hist_vec);
        
        // // Print the histograms after sorting and adding
        // for (auto& hist : hist_vec) {
        //     std::cout << "  " << hist.first->GetName() << " Full name: " << hist.second << " Integral = " << hist.first->Integral() << std::endl;
        // }
    }

    return all_histos;
}

void plot_histograms(const std::map<std::string, std::vector<std::pair<TH1F*, std::string>>>& signal_histograms,
                     const std::map<std::string, std::vector<std::pair<TH1F*, std::string>>>& background_histograms, 
                     bool logY = false) {

    int backgroundFillStyle = 1001; // Change this according to the desired fill style

    for (const auto& pair : signal_histograms) {
        const std::string& name = pair.first;
        // std::cout << "Plotting histo: " << name << std::endl;

        auto signal_vec = pair.second;
        auto background_vec = background_histograms.at(name);

        // Create a canvas for each pair of histograms
        TCanvas* canvas = new TCanvas(name.c_str(), "MyPlot", 800, 600);
        canvas->cd();

        // Add a legend
        TLegend* legend = new TLegend(0.7, 0.7, 0.9, 0.9);
        legend->SetBorderSize(0);

        // int colorIndexBkg = 2; //Background color: start from red
        int styleIndexSig = 1; //Signal line style: start from solid
        Double_t maxBinContent = 0.0;

        // First loop over backgrounds since they are larger
        for(auto& pair : background_vec) {
            TH1F* background = pair.first;
            std::string label = parse_process_name(pair.second);
            
            background->SetLineColor(kBlack);
            background->SetLineWidth(1);
            if(label.find("DrellYan") != std::string::npos) {
                background->SetFillColor(801);
            }
            else if(label.find("Top") != std::string::npos) {
                background->SetFillColor(417);
            }
            else if(label.find("Diboson") != std::string::npos) {
                background->SetFillColor(617);
            }
            // background->SetFillColor(colorIndexBkg);
            background->SetFillStyle(backgroundFillStyle);
            background->Draw("HIST SAME");

            maxBinContent = TMath::Max(maxBinContent, background->GetMaximum());
            if(logY) 
                background->GetYaxis()->SetRangeUser(0.001, maxBinContent * 100);
            else
                background->GetYaxis()->SetRangeUser(0., maxBinContent * 1.2);

            // Add background to the legend
            legend->AddEntry(background, label.c_str(), "f");
            // colorIndexBkg++;
        }

        // Then loop over signals
        for(auto& pair : signal_vec) {
            TH1F* signal = pair.first;
            std::string label = parse_process_name(pair.second);

            signal->GetYaxis()->SetRangeUser(0.001, maxBinContent * 100);
            signal->SetLineColor(1);
            signal->SetLineWidth(3);
            signal->SetLineStyle(styleIndexSig);
            signal->Draw("HIST SAME");

            // Add signal to the legend
            legend->AddEntry(signal, label.c_str(), "l");
            styleIndexSig++;
        }

        // Set log scale if requested
        if (logY) canvas->SetLogy();
        
        legend->Draw();
        canvas->Update();
        canvas->RedrawAxis();
        canvas->SaveAs(("../plots/PlotMaker_" + name + ".png").c_str());
        canvas->SaveAs(("../plots/PlotMaker_" + name + ".pdf").c_str());
    }
}

void plot_histograms_2D(const std::map<std::string, TH2F*>& histograms) {
    // For each histogram in the map, create a canvas and draw the histogram
    for (const auto& pair : histograms) {
        std::string canvas_name = "canvas_" + pair.first;
        TCanvas* canvas = new TCanvas(canvas_name.c_str(), "MyPlot", 800, 600);
        
        // Draw the histogram with color palette
        pair.second->Draw("COLZ");
        
        canvas->Update();
        canvas->SaveAs(("../plots/PlotMaker_" + pair.first + ".png").c_str());
        canvas->SaveAs(("../plots/PlotMaker_" + pair.first + ".pdf").c_str());
    }
}

void PlotMaker2(bool logY = true){

    InitializeROOT();

    std::vector<std::string> signal_files = { "../histos/VBF_Run3_output.root", "../histos/ggH_Run3_output.root" };  // list of signal file names
    std::vector<std::string> background_files = { "../histos/DrellYan_Run3_output.root", "../histos/Top_Run3_output.root", "../histos/Diboson_Run3_output.root" };  // list of background file names

    std::vector<TFile*> signal_tfiles = open_files(signal_files);
    std::vector<TFile*> background_tfiles = open_files(background_files);

    auto signal_histos = collect_histos(signal_tfiles);
    auto background_histos = collect_histos(background_tfiles);
    
    plot_histograms(signal_histos, background_histos, logY);

    auto signal_maps = collect_maps(signal_tfiles);
    auto background_maps = collect_maps(background_tfiles);
    
    plot_histograms_2D(signal_maps);
    plot_histograms_2D(background_maps);
}
