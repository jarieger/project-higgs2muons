#!/bin/bash

if [[ -z "$1" ]]; then
  ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
  ALRB_localConfigDir=/project/atlas/nikhef/cvfms/config
  lsetup "views LCG_101 x86_64-centos7-gcc8-opt"
fi

if [[ "$1" == "--LesCenter" ]]; then
  source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.24.08/x86_64-centos7-gcc48-opt/bin/thisroot.sh
fi

if [[ "$1" == "--combine" ]]; then
  ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
  ALRB_localConfigDir=/project/atlas/nikhef/cvfms/config
  lsetup "views LCG_102 x86_64-centos7-gcc11-opt"
  export PATH=${PATH}:${PWD}/HiggsAnalysis/CombinedLimit/build/bin
  export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PWD}/HiggsAnalysis/CombinedLimit/build/lib
  export PYTHONPATH=${PYTHONPATH}:${PWD}/HiggsAnalysis/CombinedLimit/build/lib/python
fi
