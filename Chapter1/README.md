# Chapter 1. 
# Concept of detector resolution and mass reconstruction from decay products

This chapter is dedicated to learn how to extract information from the simulated data. First, we will investiage how precise the detector is able to measure different particle properties. Second, we will learn how to calculate the mass of a particle based on the kinematic properties of its decay products.

### Goals
- Calculate the invariant mass of the Higgs boson based on the kinematic properties of the muons. Identify differences compared to the Z boson.
- Measure the detector resolution for the transverse momentum pT, the angle Eta (longitudnal direction), and the angle Phi (transversal direction) for both muons.
<!-- **M=sqrt(2*pT1*pT2(cosh(Eta1-Eta2)-cos(Phi1-Phi2)))** -->

Before we go ahead we should have a look at the *ROOT* macros that have been prepared to analyze the NTuples. **Go to [ROOTmacros](ROOTmacros).**

## 1.1. Invariant mass of the Higgs boson

The Higgs boson mass can be calculated by the energy E and momentum p of the muons. In particle collider experiments, one often defines the angular position of a particle in terms of an azimuthal angle Phi and pseudorapidity Eta, and specifies its momentum in the transverse plane pT. In the case of highly relativistic particles (E>>m) a simplified expression of the invariant mass can be derived.

    M=sqrt(2*pT1*pT2(cosh(DeltaEta)-cos(DeltaPhi)))

## 1.2. Reconstruction-level and Truth properties 

The measured properties (reconstruction-level) of the decay products are as precise as the detector is capable to measure them. Very precise detectors provide very accurate measuremetns of the true properties. However, even the best detector is not able to measure without any uncertainty. Therefore, measured values can differ from the true values. An advantage of simulated data is that it provides access to the *truth* values of particle properties without imprecisions of the measurements. 

<!-- **Goal:** The impact of the uncertainty in the muon momentum measurement is to be studied in this chapter. For this purpose, a set of *ROOT macros* have been prepared. Follow the instructions below. -->
<!-- two pre-compiled ROOT macros can be launched. They are located in the directory [macros](macros). --> 


## Exercise 1 - Calculate the invariant mass

a) Select event No. 10, No. 100 and No. 1000 of the `GGF` NTuple and calculate the invariant mass of a Higgs boson decaying to muons based on the truth values.

    my_Truth_PT_Lead_Muons, my_Truth_Eta_Lead_Muons, my_Truth_Phi_Lead_Muons, my_Truth_PT_Sub_Muons, my_Truth_Eta_Sub_Muons, my_Truth_Phi_Sub_Muons
    
Compare the values to the one stored in the NTuple `Truth_Minv_MuMu`.

b) Do the same for the background from Z boson production. Use the `DY` NTuple for this (see [data](../data/)).

## Exercise 2 - Compare Higgs and Z boson events 

a) Plot the true invariant mass distribution for Higgs boson events and Z boson events. 

b) Take a look into the literatur and compare the mean value of the distribution to the literatur value of the respective boson mass.

## Exercise 3 - Reconstruction of invariant mass and detector resolution

a) Plot the reconstructed mass distribution for the Higgs boson and Z boson events. Compare these distributions to the ones obtained in 2a). How do these distributions differ? Comment on their shape, mean value and width. To investiagte the shape use `SmartFit`.

b) Investigate the detector resolution of the quantities that are used to calculate the invariant mass: pT, Eta, Phi. For this purpose it is helpful to plot the ratio of the truth values and the reconstructed values. Use `QuickPlot`. Analyse the ratio using `SmartFitImport`. Comment on the results, and compare the resolution of pT, Eta and Phi. 

c) Investigate the detector resolution of the invariant mass of the Higgs boson.

## Exercise 4 - Summary

Summarize your findings: What are differences between the Higgs boson and the Z boson? How does the measurement change the true values of pT, Eta, and Phi? Identify the quantity with the largest uncertainty. Compare it to the uncertainty of the invarinat mass in 3c).