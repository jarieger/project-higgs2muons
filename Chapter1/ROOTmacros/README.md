# Getting familiar with ROOT Macros

## EventLoop.C

In the beginning it is helpful to have a look at particle properties in single events. Therefore, a *ROOT* macro called `EventLoop` has been prepared. This macro can be used to loop over the events in the NTuple and extract values for different muon properties. The output can also be visualized by histograms.

**List of properties:**
Muons_PT_Lead, Muons_Eta_Lead, Muons_Phi_Lead, Muons_PT_Sub, Muons_Eta_Sub, Muons_Phi_Sub, Muons_Minv_MuMu, Truth_PT_Lead_Muon, Truth_Eta_Lead_Muon, Truth_Phi_Lead_Muon, Truth_PT_Sub_Muon, Truth_Eta_Sub_Muon, Truth_Phi_Sub_Muon, Truth_Minv_MuMu.

- Open a terminal and navigate to the `ROOTmacros` directory. 
- Start *ROOT*, compile and launch the `EventLoop` macro:

        root
        .L EventLoop.C
        EventLoop()

- The `EventLoop` macro can take the following arguments: 

        EventLoop(Int_t <number of events>, TString <branch name>, TString <dataset identifier>)

- Try to change the number of analyzed events:

        EventLoop(<number of events>)

    *Example:*

        EventLoop(1)
        EventLoop(100)
        EventLoop(10000)

- If you want to visualize the output of this script you can enable this by adding the second argument:

        EventLoop(<number of events>,<branch name>)
    
    *Example:*
    
        EventLoop(1000,"Muons_PT_Lead")

- In order to change to another dataset, you have to enable the third arguement:

        EventLoop(<number of events>,<branch name>,<dataset identifier>)

    *Options for `dataset identifier` :*
    1. GGF (default)
    2. VBF (different Higgs boson production process)
    3. DY (background process)
    
    *Example:*
    
        EventLoop(1000,"Muons_PT_Lead","DY")

## QuickPlot.C

Another *ROOT* macro that can be used to analyze and vizualize the data is `QuickPlot`. As the name says, this macro is designed to quickly produce histograms of all the different properties saved in the NTuples. `QuickPlot` saves these histograms automatically as a `.pdf` file in [plots](../plots). In addition, you can export the histogram as another root file. The exported file will be automatically saved to [output](../output).

- Open a terminal and navigate to the `ROOTmacros` directory. 
- Start *ROOT*, compile and launch the `QuickPlot` macro:

        root
        .L QuickPlot.C
        QuickPlot()

- The `QuickPlot` macro can take the following arguments: 

        QuickPlot(TString <branch name>, Int <number of bins>, Double <min>, Double <max>, TString <selection>, TString <selectionName>, bool <saveExport>, TString <dataset identifier>)

- If you want to visualize any of the properties saved in the NTuple, you just have the provide the branch name (see [data](../../data/)) and `QuickPlot` will print the histogram and save it as a `.pdf` file to [plots](../plots). 

        QuickPlot(<branch name>, <number of bins>, <xmin>, <xmax>)
    
    *Example:*
    
        QuickPlot("Muons_PT_Lead", 100, 0, 100)

    *This will plot the muon pT of the muon with the higher pT in the event in 100 bins from 0 GeV to 100 GeV.*

- Furthermore you can specify a selection. If you want to select only events with a very high pT muon, you have to do the following:

        QuickPlot(<branch name>, <number of bins>, <xmin>, <xmax>, <selection>, <selectionName>)

    *Example:*

        QuickPlot("Muons_PT_Lead", 100, 0, 200, "Muons_PT_Lead>100","highPT")

    You can add more than one criterium like this:

        QuickPlot("Muons_PT_Lead", 100, 0, 200, "Muons_PT_Lead>100 && Muons_PT_Sub>100", "bothMuons_highPT")

    *Selection logic:* 
    1. && `and`
    2. || `or`

    Moreover you can compare two distributions. To calculate the ratio:

        QuickPlot("Muons_PT_Lead/Truth_PT_Lead_Muon", 100, 0.9, 1.1)

    or a difference:

        QuickPlot("Muons_PT_Lead-Truth_PT_Lead_Muon", 100, -5, 5)

    **Note** that you can give also a `selectionName`. This name will be appended to your output file to be able to identify them quickly. The default value is `selectedEvents`.

- Finaly, the last option corresponds to the export function of `QuickPlot`. This will allow you to export the histogram as a `.root` file. At some point you might want to use this as an input to another macro. 

        QuickPlot(<branch name>, <number of bins>, <xmin>, <xmax>,<selection>, <selectionName>, <saveExport>)

    *Example:*

        QuickPlot("Muons_PT_Lead", 100, 0, 200, "Muons_PT_Lead>100 && Muons_PT_Sub>100", "bothMuons_highPT", true)

## SmartFit.C

The last *ROOT* macro `SmartFit` can be used to extract information from the analyzed histograms. `SmartFit` can fit different peak shapes. You are probably already familiar with the [Gaussian](https://de.wikipedia.org/wiki/GAUSSIAN) distribution. Furthermore, you can fit the histograms using a [CrystalBall](https://en.wikipedia.org/wiki/Crystal_Ball_function) function, a double-sided Crystal Ball, or a [Breit-Wigner](https://en.wikipedia.org/wiki/Cauchy_distribution) distribution. `SmartFit` provides as output the mean and width of the fit model, and saves the plot as a `.pdf` file in [plots](../plots). 

- Open a terminal and navigate to the `ROOTmacros` directory. 
- Start *ROOT*, compile and launch the `SmartFit` macro:

        root
        .L SmartFit.C
        SmartFit()

- The `SmartFit` macro can take the following arguments: 

        SmartFit(TString <fit model>, TString <branch name>, Int <number of bins>, Double <min>, Double <max>, TString <dataset identifier>)

- The first arguemnt is the fit model. You can choose between `Gaussian`, `CrystalBall`, `DoubleCB`, `BreitWigner`, `Voigt`.
- With the second argument you can select the branch name (see [data](../../data/)).
- The third to fifth argument is used to select the binning of the input distribution, as well as the minimum and maximum of the x axis.

    *Examples:*

        SmartFit("Gaussian", "Muons_Minv_MuMu", 50, 110, 135)
        SmartFit("CrystalBall", "Muons_Minv_MuMu", 50, 110, 135)
        
- Change the binning of the input:

    *Examples:*

        SmartFit("Gaussian", "Muons_Minv_MuMu", 50, 110, 135)
        SmartFit("Gaussian", "Muons_Minv_MuMu", 100, 110, 135)
        SmartFit("Gaussian", "Muons_Minv_MuMu", 25, 110, 135)

- Change the range of the x axis:

    *Examples:*

        SmartFit("Gaussian", "Muons_Minv_MuMu", 50, 100, 150)
        SmartFit("Gaussian", "Muons_Minv_MuMu", 50, 110, 135)
        SmartFit("Gaussian", "Muons_Minv_MuMu", 30, 125, 130)

## SmartFitImport.C

In order to be able to analyze histograms that are exported using `QuickPlot`, a new *ROOT* macro derived from `SmartFit` is provided. It is called `SmartFitImport`. Similar to `SmartFit` it can be used to extract information from histograms. `SmartFitImport` can fit the same set of peak shapes. The ony difference is that it takes as input the exported histograms from `QuickPlot`.

- Open a terminal and navigate to the `ROOTmacros` directory. 
- Start *ROOT* and compile `SmartFitImport` macro:

        root
        .L SmartFitImport.C

- The `SmartFitImport` macro can take the following arguments: 

        SmartFitImport(TString <fit model>, TString <input filename>)

- The first arguemnt is the fit model. You can choose between `Gaussian`, `CrystalBall`, `doubleCB`, `BreitWigner`, `Voigt`.
- With the second argument you select the input file that was created using `QuickPlot`. 

    *Examples:*

        SmartFitImport("Gaussian", "GGF_Muons_PT_Lead_diff_Truth_PT_Lead_Muon.root")
        SmartFitImport("Gaussian", "GGF_Muons_PT_Lead.root")
        
**Go back to [Chapter 1](../../Chapter1/).**
