#include <iostream>
#include <vector>

#include "TFile.h"
#include "TTree.h"
#include "TCut.h"
#include "TString.h"
#include "TSystem.h"

class TProgressBar
{
public:
    TProgressBar(const char *title, Int_t nbins)
        : fLabel(title), fBins(nbins), fCurrent(0) {}

    void SetRange(Int_t min, Int_t max)
    {
        fMin = min;
        fMax = max;
    }

    void Update(Int_t value)
    {
        Int_t newValue = static_cast<Int_t>((static_cast<Float_t>(value - fMin) / static_cast<Float_t>(fMax - fMin)) * fBins);
        while (newValue > fCurrent)
        {
            fCurrent++;
            if (fCurrent % (fBins / 50) == 0 || fCurrent == fBins)
            {
                int progress = (fCurrent * 100) / fBins;
                std::cout << "\r" << fLabel << " [" << std::string(progress / 2, '=') << "> " << std::string(50 - progress / 2, ' ') << "] " << progress
                          << "%";
                std::cout.flush();
            }
        }
    }

private:
    TString fLabel;
    Int_t fBins;
    Int_t fMin;
    Int_t fMax;
    Int_t fCurrent;
};

void copytree2()
{
    Int_t nEntries = -1;
    TCut selection = "Muons_Minv_MuMu>105 && Muons_Minv_MuMu<180";
    // std::vector<TString> fileNames = {"VBF_000001.tree_output.root"};
    // std::vector<TString> fileNames = {"ggH_000001.tree_output.root"};
    // std::vector<TString> fileNames = {"Diboson_000001.tree_output.root"};
    std::vector<TString> fileNames = {"ttbar_000001.tree_output.root"};
    // std::vector<TString> fileNames = {
    //     "Zmumu_000001.tree_output.root",
    //     "Zmumu_000002.tree_output.root",
    //     "Zmumu_000003.tree_output.root",
    //     "Zmumu_000004.tree_output.root",
    //     "Zmumu_000005.tree_output.root",
    //     "Zmumu_000006.tree_output.root",
    //     "Zmumu_000007.tree_output.root",
    //     "Zmumu_000008.tree_output.root",
    //     "Zmumu_000009.tree_output.root",
    //     "Zmumu_000010.tree_output.root",
    //     "Zmumu_000011.tree_output.root"};

    // Input/output paths and tree name
    // TString pathName = "/data/atlas/users/orieger/H2mu/RunIII/Zmumu_PhPy8/";
    // TString pathName = "/data/atlas/users/orieger/H2mu/RunIII/VBF/";
    // TString pathName = "/data/atlas/users/orieger/H2mu/RunIII/ggH/";
    // TString pathName = "/data/atlas/users/orieger/H2mu/RunIII/Diboson/";
    TString pathName = "/data/atlas/users/orieger/H2mu/RunIII/ttbar/";
    TString treeName = "tree_Hmumu";

    TProgressBar pb("Progress", 100); // Second argument is the number of '=' characters representing 100% progress
    pb.SetRange(0, fileNames.size() - 1);
    Int_t progressStep = 0;

    for (auto fileName : fileNames)
    {
        pb.Update(progressStep);

        ///////////////////////
        // Get and open file //
        ///////////////////////

        TString fullName = pathName + fileName;
        TFile *oldFile = new TFile(fullName, "READ");
        TTree *oldTree = (TTree *)oldFile->Get(treeName);

        oldTree->SetBranchStatus("*", 0);
        // oldTree->SetBranchStatus("MetaData_IsFullSim", 1);
        oldTree->SetBranchStatus("run", 1);
        // oldTree->SetBranchStatus("EventInfo_LumiBlock", 1);
        oldTree->SetBranchStatus("event", 1);
        // oldTree->SetBranchStatus("EventInfo_npv", 1);
        // oldTree->SetBranchStatus("EventInfo_RndRunNumber", 1);
        // oldTree->SetBranchStatus("Event_AverageMu", 1);
        // oldTree->SetBranchStatus("Event_ActualMu", 1);
        // oldTree->SetBranchStatus("Event_CorrAverageMu", 1);
        // oldTree->SetBranchStatus("Event_CorrActualMu", 1);
        // oldTree->SetBranchStatus("Event_RandomVal", 1);
        // oldTree->SetBranchStatus("Truth_Boson_Mass", 1);
        // oldTree->SetBranchStatus("EventInfo_PassTrigger", 1);
        // oldTree->SetBranchStatus("EventInfo_PassTriggerMatching", 1);
        // oldTree->SetBranchStatus("EventInfo_PassElectronTrigger", 1);
        // oldTree->SetBranchStatus("EventInfo_PassElectronTriggerMatch", 1);
        // oldTree->SetBranchStatus("EventInfo_Pass_HLT_xe70_mht", 1);
        // oldTree->SetBranchStatus("EventInfo_Pass_HLT_xe90_mht_L1XE50", 1);
        // oldTree->SetBranchStatus("EventInfo_Pass_HLT_xe110_mht_L1XE50", 1);
        // oldTree->SetBranchStatus("EventInfo_Pass_HLT_xe110_pufit_L1XE55", 1);
        // oldTree->SetBranchStatus("EventInfo_Pass_HLT_xe110_pufit_xe70_L1XE50", 1);
        oldTree->SetBranchStatus("Muons_Charge_Lead", 1);
        oldTree->SetBranchStatus("Muons_Charge_Sub", 1);
        oldTree->SetBranchStatus("Muons_Type_Lead", 1);
        oldTree->SetBranchStatus("Muons_Type_Sub", 1);
        // oldTree->SetBranchStatus("Muons_Multip", 1);
        // oldTree->SetBranchStatus("Muons_TruthOrigin_Lead", 1);
        // oldTree->SetBranchStatus("Muons_TruthOrigin_Sub", 1);
        // oldTree->SetBranchStatus("Muons_TruthType_Lead", 1);
        // oldTree->SetBranchStatus("Muons_TruthType_Sub", 1);
        oldTree->SetBranchStatus("Muons_Minv_MuMu", 1);
        oldTree->SetBranchStatus("Muons_PT_Lead", 1);
        oldTree->SetBranchStatus("Muons_PT_Sub", 1);
        oldTree->SetBranchStatus("Muons_Eta_Lead", 1);
        oldTree->SetBranchStatus("Muons_Eta_Sub", 1);
        oldTree->SetBranchStatus("Muons_Phi_Lead", 1);
        oldTree->SetBranchStatus("Muons_Phi_Sub", 1);
        oldTree->SetBranchStatus("Muons_Minv_MuMu_Fsr", 1);
        oldTree->SetBranchStatus("Muons_Minv_MuMu_Sigma", 1);
        oldTree->SetBranchStatus("Muons_Minv_MuMu_Fsr_Sigma", 1);
        oldTree->SetBranchStatus("Muons_DeltaEta_MuMu", 1);
        oldTree->SetBranchStatus("Muons_DeltaPhi_MuMu", 1);
        oldTree->SetBranchStatus("Muons_DeltaR_MuMu", 1);
        oldTree->SetBranchStatus("Muons_CosThetaStar", 1);
        oldTree->SetBranchStatus("FSR_Et", 1);
        oldTree->SetBranchStatus("FSR_Eta", 1);
        oldTree->SetBranchStatus("FSR_Phi", 1);
        oldTree->SetBranchStatus("FSR_f1", 1);
        oldTree->SetBranchStatus("FSR_DeltaR", 1);
        oldTree->SetBranchStatus("FSR_Type", 1);
        oldTree->SetBranchStatus("FSR_IsPhoton", 1);
        oldTree->SetBranchStatus("Z_PT", 1);
        oldTree->SetBranchStatus("Z_Y", 1);
        oldTree->SetBranchStatus("Z_Eta", 1);
        oldTree->SetBranchStatus("Z_Phi", 1);
        oldTree->SetBranchStatus("Z_PT_FSR", 1);
        oldTree->SetBranchStatus("Z_Y_FSR", 1);
        oldTree->SetBranchStatus("Z_Eta_FSR", 1);
        oldTree->SetBranchStatus("Z_Phi_FSR", 1);
        oldTree->SetBranchStatus("Event_MET", 1);
        oldTree->SetBranchStatus("Event_MET_Phi", 1);
        oldTree->SetBranchStatus("Event_MET_Sig", 1);
        oldTree->SetBranchStatus("Event_MET_Sig_CP", 1);
        oldTree->SetBranchStatus("Event_Ht", 1);
        oldTree->SetBranchStatus("Event_HasBJet", 1);
        // oldTree->SetBranchStatus("Truth_QoverP_Lead_Muon", 1);
        // oldTree->SetBranchStatus("Truth_QoverP_Sub_Muon", 1);
        // oldTree->SetBranchStatus("Truth_PT_Muons", 1);
        // oldTree->SetBranchStatus("Truth_Eta_Muons", 1);
        // oldTree->SetBranchStatus("Truth_Phi_Muons", 1);
        // oldTree->SetBranchStatus("Truth_Status_Muons", 1);
        oldTree->SetBranchStatus("Jets_jetMultip", 1);
        oldTree->SetBranchStatus("Jets_PT", 1);
        oldTree->SetBranchStatus("Jets_Eta", 1);
        oldTree->SetBranchStatus("Jets_Phi", 1);
        oldTree->SetBranchStatus("Jets_E", 1);
        // oldTree->SetBranchStatus("Jets_PassFJVT", 1);
        // oldTree->SetBranchStatus("Jets_LowestPassedBTagOP", 1);
        oldTree->SetBranchStatus("Jets_Minv_jj", 1);
        oldTree->SetBranchStatus("Jets_PT_jj", 1);
        oldTree->SetBranchStatus("Jets_PT_Lead", 1);
        oldTree->SetBranchStatus("Jets_PT_Sub", 1);
        oldTree->SetBranchStatus("Jets_Eta_Lead", 1);
        oldTree->SetBranchStatus("Jets_Eta_Sub", 1);
        oldTree->SetBranchStatus("Jets_Phi_Lead", 1);
        oldTree->SetBranchStatus("Jets_Phi_Sub", 1);
        oldTree->SetBranchStatus("Jets_E_Lead", 1);
        oldTree->SetBranchStatus("Jets_E_Sub", 1);
        oldTree->SetBranchStatus("Jets_PassFJVT_Lead", 1);
        oldTree->SetBranchStatus("Jets_PassFJVT_Sub", 1);
        // oldTree->SetBranchStatus("Jets_PT_Lead_Truth", 1);
        // oldTree->SetBranchStatus("Jets_PT_Sub_Truth", 1);
        // oldTree->SetBranchStatus("Jets_Eta_Lead_Truth", 1);
        // oldTree->SetBranchStatus("Jets_Eta_Sub_Truth", 1);
        // oldTree->SetBranchStatus("Jets_Phi_Lead_Truth", 1);
        // oldTree->SetBranchStatus("Jets_Phi_Sub_Truth", 1);
        // oldTree->SetBranchStatus("Jets_E_Lead_Truth", 1);
        // oldTree->SetBranchStatus("Jets_E_Sub_Truth", 1);
        // oldTree->SetBranchStatus("Jets_TruthDR_Lead", 1);
        // oldTree->SetBranchStatus("Jets_TruthDR_Sub", 1);
        oldTree->SetBranchStatus("Jets_DeltaR_jj", 1);
        oldTree->SetBranchStatus("Jets_DeltaEta_jj", 1);
        oldTree->SetBranchStatus("Jets_DeltaPhi_jj", 1);
        oldTree->SetBranchStatus("Jets_Eta_jj", 1);
        oldTree->SetBranchStatus("Jets_Phi_jj", 1);
        // oldTree->SetBranchStatus("Jets_NumTrkPt500_Lead", 1);
        // oldTree->SetBranchStatus("Jets_NumTrkPt500_Sub", 1);
        // oldTree->SetBranchStatus("Jets_NumTrkPt1000_Lead ", 1);
        // oldTree->SetBranchStatus("Jets_NumTrkPt1000_Sub", 1);
        // oldTree->SetBranchStatus("Jets_SumPtTrkPt500_Lead", 1);
        // oldTree->SetBranchStatus("Jets_SumPtTrkPt500_Sub", 1);
        // oldTree->SetBranchStatus("Jets_TrackWidthPt1000_Lead", 1);
        // oldTree->SetBranchStatus("Jets_TrackWidthPt1000_Sub", 1);
        // oldTree->SetBranchStatus("Jets_PartonID_Lead", 1);
        // oldTree->SetBranchStatus("Jets_PartonID_Sub", 1);
        oldTree->SetBranchStatus("Jets_NTracks_Lead", 1);
        oldTree->SetBranchStatus("Jets_NTracks_Sub", 1);
        // oldTree->SetBranchStatus("Jets_TracksWidth_Lead", 1);
        // oldTree->SetBranchStatus("Jets_TracksWidth_Sub", 1);
        // oldTree->SetBranchStatus("Jets_TracksC1_Lead", 1);
        // oldTree->SetBranchStatus("Jets_TracksC1_Sub", 1);
        // oldTree->SetBranchStatus("Jets_Weight_TrackEfficiency_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_TrackFakes_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_NChargedExp_Up_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_NChargedExp_Down_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_NChargedME_Up_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_NChargedME_Down_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_NChargedPDF_Up_Lead", 1);
        // oldTree->SetBranchStatus("Jets_Weight_NChargedPDF_Down_Lead", 1);
        // oldTree->SetBranchStatus("Jets_QGTag_BDTScore_Lead", 1);
        // oldTree->SetBranchStatus("Jets_QGTag_BDTScore_Sub", 1);
        oldTree->SetBranchStatus("Event_Centrality", 1);
        oldTree->SetBranchStatus("Event_PT_MuMuj1", 1);
        oldTree->SetBranchStatus("Event_PT_MuMuj2", 1);
        oldTree->SetBranchStatus("Event_PT_MuMujj", 1);
        oldTree->SetBranchStatus("Event_Y_MuMuj1", 1);
        oldTree->SetBranchStatus("Event_Y_MuMuj2", 1);
        oldTree->SetBranchStatus("Event_Y_MuMujj", 1);
        oldTree->SetBranchStatus("Truth_PT_Lead_Muon", 1);
        oldTree->SetBranchStatus("Truth_PT_Sub_Muon", 1);
        oldTree->SetBranchStatus("Truth_Eta_Lead_Muon", 1);
        oldTree->SetBranchStatus("Truth_Eta_Sub_Muon", 1);
        oldTree->SetBranchStatus("Truth_Phi_Lead_Muon", 1);
        oldTree->SetBranchStatus("Truth_Phi_Sub_Muon", 1);
        // oldTree->SetBranchStatus("Truth_Status_Lead_Muon", 1);
        // oldTree->SetBranchStatus("Truth_Status_Sub_Muon", 1);
        oldTree->SetBranchStatus("Truth_Minv_MuMu", 1);
        // oldTree->SetBranchStatus("Higgs_Kid_AbsPdgID", 1);
        // oldTree->SetBranchStatus("Muons_Pos_PT", 1);
        // oldTree->SetBranchStatus("Muons_Pos_Eta", 1);
        // oldTree->SetBranchStatus("Muons_Pos_Phi", 1);
        // oldTree->SetBranchStatus("Muons_Pos_E", 1);
        // oldTree->SetBranchStatus("Muons_Pos_IsoSFRatio", 1);
        oldTree->SetBranchStatus("Muons_Pos_PassTightIso", 1);
        // oldTree->SetBranchStatus("Muons_Neg_PT", 1);
        // oldTree->SetBranchStatus("Muons_Neg_Eta", 1);
        // oldTree->SetBranchStatus("Muons_Neg_Phi", 1);
        // oldTree->SetBranchStatus("Muons_Neg_E", 1);
        // oldTree->SetBranchStatus("Muons_Neg_IsoSFRatio", 1);
        oldTree->SetBranchStatus("BtagSFWeight", 1);
        oldTree->SetBranchStatus("Muons_Neg_PassTightIso", 1);
        oldTree->SetBranchStatus("TriggerSFWeight", 1);
        oldTree->SetBranchStatus("EventWeight_PileupWeight", 1);
        oldTree->SetBranchStatus("MuonSFWeight", 1);
        oldTree->SetBranchStatus("JetSFWeight", 1);
        oldTree->SetBranchStatus("TotalWeight", 1);
        oldTree->SetBranchStatus("EventWeight_MCEventWeight", 1);
        // oldTree->SetBranchStatus("Electrons_Multip", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_PT", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_Eta", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_Phi", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_E", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_SFWeight", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_TriggerSF", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_IsoSFRatio", 1);
        // oldTree->SetBranchStatus("Electrons_Pos_PassTightIso", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_PT", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_Eta", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_Phi", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_E", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_SFWeight", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_TriggerSF", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_IsoSFRatio", 1);
        // oldTree->SetBranchStatus("Electrons_Neg_PassTightIso", 1);

        /////////////////////////////
        // Create output file name //
        /////////////////////////////
        Ssiz_t pos = fileName.First('.');
        if (pos != kNPOS)
            fileName.Remove(pos);
        TString outFile = fileName + "_copytree2output.root";

        ///////////////////
        // Concatenate Trees in Output Files //
        ///////////////////
        TFile *newFile;

        if (gSystem->AccessPathName(outFile))
            newFile = new TFile(outFile, "RECREATE");
        else
            newFile = new TFile(outFile, "UPDATE");

        TTree *newTree;

        if (selection.Sizeof() > 1)
        {
            newTree = oldTree->CopyTree(selection, "");
        }
        else
        {
            newTree = oldTree->CloneTree();
        }

        // Write output files
        newFile->cd();
        newTree->Write("", TObject::kOverwrite);
        newFile->Close();

        delete oldFile;
        delete newFile;

        progressStep++;
    }

    pb.Update(fileNames.size()); // Set progress bar at 100% after loop completes
}
