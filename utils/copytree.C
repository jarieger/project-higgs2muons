#include <iostream>

#include "TH1F.h"
#include "TPad.h"
#include "TRandom.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TLatex.h"
#include "TCut.h"
#include "TLegend.h"
#include "TString.h"

void copytree(Int_t nEntries=100000, TString sampleName = "GGF", TString selection = "Muons_Minv_MuMu>105 && Muons_Minv_MuMu<180") {

    //////////////
    //Get TTree //
    //////////////

    TString pathName= "/data/atlas/users/orieger/H2mu/";
    TString treeName= "DiMuonNtuple";
    TString fileName= "";
    
    // Select sample
    if(sampleName=="GGF")
        fileName= "hmumu_test_1M.root";
    else if(sampleName=="VBF")
        fileName= "VBF_test_1M.root";
    else if(sampleName=="DY")
        fileName= "DY_test_4M.root";
    else
        std::cout<<"No correct input tree!!"<<std::endl;
    
    TString fullName = pathName + fileName;
    TFile* oldFile = new TFile(fullName, "READ");
    TTree* oldTree = (TTree*) oldFile->Get(treeName);

    ////////////////
    // EventLoop //
    ///////////////
    
    oldTree->SetBranchStatus("*",0);
    // oldTree->SetBranchStatus("EventInfo_RunNumber",1);
    // oldTree->SetBranchStatus("EventInfo_EventNumber",1);
    // oldTree->SetBranchStatus("EventInfo_PassTrigger",1);
    // oldTree->SetBranchStatus("EventInfo_PassTriggerMatching",1);
    // oldTree->SetBranchStatus("Event_AverageMu",1);
    // oldTree->SetBranchStatus("Event_ActualMu",1);
    oldTree->SetBranchStatus("EventWeight_PileupWeight",1);
    oldTree->SetBranchStatus("EventWeight_MCEventWeight",1);
    oldTree->SetBranchStatus("Muons_PT_Lead",1);
    oldTree->SetBranchStatus("Muons_PT_Sub",1);
    oldTree->SetBranchStatus("Muons_Eta_Lead",1);
    oldTree->SetBranchStatus("Muons_Eta_Sub",1);
    oldTree->SetBranchStatus("Muons_Phi_Lead",1);
    oldTree->SetBranchStatus("Muons_Phi_Sub",1);
    oldTree->SetBranchStatus("Muons_Charge_Lead",1);
    oldTree->SetBranchStatus("Muons_Charge_Sub",1);
    // oldTree->SetBranchStatus("Muons_Multip",1);
    oldTree->SetBranchStatus("Muons_Minv_MuMu",1);
    oldTree->SetBranchStatus("Muons_Minv_MuMu_Sigma",1);
    oldTree->SetBranchStatus("Muons_DeltaEta_MuMu",1);
    oldTree->SetBranchStatus("Muons_DeltaPhi_MuMu",1);
    oldTree->SetBranchStatus("Muons_DeltaR_MuMu",1);
    oldTree->SetBranchStatus("Muons_CosThetaStar",1);
    // oldTree->SetBranchStatus("Jets_E_Lead",1);
    // oldTree->SetBranchStatus("Jets_E_Sub",1);
    // oldTree->SetBranchStatus("Jets_PT_Lead",1);
    // oldTree->SetBranchStatus("Jets_PT_Sub",1);
    // oldTree->SetBranchStatus("Jets_Eta_Lead",1);
    // oldTree->SetBranchStatus("Jets_Eta_Sub",1);
    // oldTree->SetBranchStatus("Jets_Phi_Lead",1);
    // oldTree->SetBranchStatus("Jets_Phi_Sub",1);
    // oldTree->SetBranchStatus("Jets_jetMultip",1);
    // oldTree->SetBranchStatus("Jets_PT_jj",1);
    // oldTree->SetBranchStatus("Jets_Eta_jj",1);
    // oldTree->SetBranchStatus("Jets_Phi_jj",1);
    // oldTree->SetBranchStatus("Jets_Minv_jj",1);
    // oldTree->SetBranchStatus("Jets_DeltaEta_jj",1);
    // oldTree->SetBranchStatus("Jets_DeltaPhi_jj",1);
    // oldTree->SetBranchStatus("Jets_DeltaR_jj",1);
    oldTree->SetBranchStatus("Z_PT",1);
    oldTree->SetBranchStatus("Z_Eta",1);
    oldTree->SetBranchStatus("Z_Phi",1);
    oldTree->SetBranchStatus("Z_Y",1);
    // oldTree->SetBranchStatus("Event_Ht",1);
    // oldTree->SetBranchStatus("Event_MET",1);
    // oldTree->SetBranchStatus("Event_Centrality",1);
    // oldTree->SetBranchStatus("Event_PT_MuMujj",1);
    // oldTree->SetBranchStatus("Event_PT_MuMuj1",1);
    // oldTree->SetBranchStatus("Event_PT_MuMuj2",1);
    // oldTree->SetBranchStatus("Event_Y_MuMujj",1);
    // oldTree->SetBranchStatus("Event_Y_MuMuj1",1);
    // oldTree->SetBranchStatus("Event_Y_MuMuj2",1);
    // oldTree->SetBranchStatus("Event_HasBJet",1);
    oldTree->SetBranchStatus("Truth_PT_Lead_Muon",1);
    oldTree->SetBranchStatus("Truth_PT_Sub_Muon",1);
    oldTree->SetBranchStatus("Truth_Eta_Lead_Muon",1);
    oldTree->SetBranchStatus("Truth_Eta_Sub_Muon",1);
    oldTree->SetBranchStatus("Truth_Phi_Lead_Muon",1);
    oldTree->SetBranchStatus("Truth_Phi_Sub_Muon",1);
    oldTree->SetBranchStatus("Truth_Status_Lead_Muon",1);
    oldTree->SetBranchStatus("Truth_Status_Sub_Muon",1);
    oldTree->SetBranchStatus("Truth_Minv_MuMu",1);
    // oldTree->SetBranchStatus("Truth_Boson_Mass",1);
    
    //////////////////////////////////////////////////////////
    // Create a new file + a clone of old tree in new file //
    /////////////////////////////////////////////////////////

    // Create ouptut
    TFile *newFile = new TFile(sampleName+"2muons.root","recreate");
    
    TTree *newTree;
    if(selection.Sizeof()>1){
        newTree = oldTree->CopyTree(selection, "", nEntries);
    }else{
        newTree = oldTree->CloneTree(nEntries);
    }

    // Write output file
    newFile->cd();
    newTree->Write("", TObject::kOverwrite);
    newFile->Close(); 

    delete oldFile;
    delete newFile;
    
}