# Project Higgs2muons

This project aims to provide insights into important features and concepts of modern particle physics analyses. Three dedicated chapters guide you through three main aspects of the analysis of **Higgs boson decays to muons** with the **ATLAS experiment**. 

**Note** This course is designed for self-studies. In case of questions or mistakes, please feel free to reach out to me via [email](mailto:oliver.rieger@nikhef.nl). 

[**Introduction to data analysis**](data)
: Theoretical background, explanations on simulated proton-proton collision data and technical information.

[**Chapter 1**](Chapter1)
: Concept of detector resolution and mass reconstruction from decay products.

[**Chapter 2**](Chapter2)
: Basics of event selection criteria to reduce backgrounds.

[**Chapter 3**](Chapter3)
: Notion of fitting functions to the data to extract the signal contribution over background events.

# Setup data analysis software

## Requirements

This course is based on the data analysis framework [*ROOT*](https://root.cern/).

> "ROOT enables statistically sound scientific analyses and visualization of large amounts of data: today, more than 1 exabyte (1,000,000,000 gigabyte) are stored in ROOT files. The Higgs was found with ROOT!" [source](https://root.cern/)

## Option 1) Log in to les-center and open a terminal

1. Go to https://les-center.nikhef.nl and log in using your credentials. 
2. After you have logged in, you can start a terminal by clicking on `Activities` in the top left of the screen and thereafter on `Terminal`. A terminal should start.

Further instructions are summarized in this [document](lesCenterInstructions.pdf).

## Option 2) Log in to Stoomboot and open a terminal
 
1. After you logged in to Stoomboot, you should navigate to your project area `cd /project/atlas/users/<username>`. Proceed with *Cloning the project* No. 3.

## Cloning the project

First of all, you need to get your personal copy of this project. This needs to be done only **once**.

1. Open a terminal.
2. Go to your home directory `cd`.
3. Enter `git clone https://gitlab.cern.ch/jarieger/project-higgs2muons.git`. 

This should start the download of the project. A new directory `project-higgs2muons` should have been created.

## Initialise ROOT 

Before you can start the *ROOT* application on the les-center remote computer you need to source *ROOT*. You need to do this whenever you start a new terminal.

1. Navigate to the project directory `cd project-higgs2muons`
2. Enter `source setup_ROOTonLesCenter.sh` or `source setup_ROOTonSTBC.sh`

## Getting started with ROOT

To analyze datasets of simulated proton-proton collisions, several *ROOT* macros have been prepared. A *ROOT* macro contains pure C++ code.

1. Open a terminal.
2. If you want to start *ROOT* you just type `root`. *Keep in mind to initialise *ROOT* in advance.*
3. After ROOT started you can compile *ROOT* macros `.L <macroname>.C` 
4. You can start a macro after compilation using `<macroname>()`. Some macros take arguments, these have to be put in the brackets. If no arguments are provided, the default values will be used.

**Proceed to [Introduction to data analysis](/data).**
