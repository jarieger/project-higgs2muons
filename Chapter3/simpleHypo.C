#include "TFile.h"
#include "RooWorkspace.h"
#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooStats/ModelConfig.h"
#include "RooRandom.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TSystem.h"
#include "TROOT.h"
 
#include "RooStats/AsymptoticCalculator.h"
#include "RooStats/HybridCalculator.h"
#include "RooStats/FrequentistCalculator.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/HypoTestPlot.h"
 
#include "RooStats/NumEventsTestStat.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/SimpleLikelihoodRatioTestStat.h"
#include "RooStats/RatioOfProfiledLikelihoodsTestStat.h"
#include "RooStats/MaxLikelihoodEstimateTestStat.h"
 
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestInverterPlot.h"
 
#include <cassert>
 
using namespace RooFit;
using namespace RooStats;

void getSignificance(TString workspaceName = "w", TString modelSBName = "sbModel", TString modelBName = "bModel", TString dataName = "obsData", bool noSystematics = false, double poiValue = -1)
{

   SimpleLikelihoodRatioTestStat::SetAlwaysReuseNLL(true);
   ProfileLikelihoodTestStat::SetAlwaysReuseNLL(true);
   RatioOfProfiledLikelihoodsTestStat::SetAlwaysReuseNLL(true);

   // Try to open the file
   TFile *file = TFile::Open("HmumuModel.root");

   // get stuff
   RooWorkspace *w = (RooWorkspace *)file->Get(workspaceName);
   if (!w)
      cout << "workspace not found" << endl;

   ModelConfig *sbModel = (ModelConfig *)w->obj(modelSBName);
   if (!sbModel)
      cout << "sb ModelConfig was not found" << endl;

   RooAbsData *data = (RooAbsData *)w->data(dataName);
   if (!data)
      cout << "data was not found" << endl;

   // make b model
   ModelConfig *bModel = (ModelConfig *)sbModel->Clone();
   bModel->SetName(TString(modelSBName) + TString("B_only"));
   RooRealVar *var = dynamic_cast<RooRealVar *>(bModel->GetParametersOfInterest()->first());
   if (!var)
      return;
   double oldval = var->getVal();
   var->setVal(0);
   bModel->SetSnapshot(RooArgSet(*var));
   var->setVal(oldval);

   if (noSystematics)
   {
      const RooArgSet *nuisPar = sbModel->GetNuisanceParameters();
      if (nuisPar && nuisPar->getSize() > 0)
      {
         std::cout << "StandardHypoTestInvDemo"
                   << "  -  Switch off all systematics by setting them constant to their initial values" << std::endl;
         RooStats::SetAllConstant(*nuisPar);
      }
      if (bModel)
      {
         const RooArgSet *bnuisPar = bModel->GetNuisanceParameters();
         if (bnuisPar)
            RooStats::SetAllConstant(*bnuisPar);
      }
   }

   if (!sbModel->GetSnapshot() || poiValue > 0) {
      Info("StandardHypoTestDemo", "Model has no snapshot  - make one using model poi");
      RooRealVar *var = dynamic_cast<RooRealVar *>(sbModel->GetParametersOfInterest()->first());
      if (!var)
         return;
      double oldval = var->getVal();
      if (poiValue > 0)
         var->setVal(poiValue);
      sbModel->SetSnapshot(RooArgSet(*var));
      if (poiValue > 0)
         var->setVal(oldval);
   }

   SimpleLikelihoodRatioTestStat *slrts = new SimpleLikelihoodRatioTestStat(*bModel->GetPdf(), *sbModel->GetPdf());
   RooArgSet nullParams(*bModel->GetSnapshot());
   if (bModel->GetNuisanceParameters())
      nullParams.add(*bModel->GetNuisanceParameters());

   slrts->SetNullParameters(nullParams);

   RooArgSet altParams(*sbModel->GetSnapshot());

   if (sbModel->GetNuisanceParameters())
      altParams.add(*sbModel->GetNuisanceParameters());

   slrts->SetAltParameters(altParams);

   ProfileLikelihoodTestStat *profll = new ProfileLikelihoodTestStat(*bModel->GetPdf());

   RatioOfProfiledLikelihoodsTestStat *ropl = new RatioOfProfiledLikelihoodsTestStat(*bModel->GetPdf(), *sbModel->GetPdf(), sbModel->GetSnapshot());
   ropl->SetSubtractMLE(false);

   HypoTestCalculatorGeneric *hypoCalc = new AsymptoticCalculator(*data, *sbModel, *bModel);

   ((AsymptoticCalculator *)hypoCalc)->SetOneSidedDiscovery(true);

   HypoTestResult *htr = hypoCalc->GetHypoTest();
   htr->SetPValueIsRightTail(true);
   htr->SetBackgroundAsAlt(false);
   htr->Print(); // how to get meaningful CLs at this point?

   delete slrts;
   delete ropl;
   delete profll;

   std::cout << "Asymptotic results " << std::endl;
   for (int i = 0; i < 5; ++i)
   {
      double sig = -2 + i;
      // sigma is inverted here
      double pval = AsymptoticCalculator::GetExpectedPValues(htr->NullPValue(), htr->AlternatePValue(), -sig, false);
      std::cout << " Expected p -value and significance at " << sig << " sigma = " << pval << " significance "
                << ROOT::Math::normal_quantile_c(pval, 1) << " sigma " << std::endl;
   }
}

void simpleHypo()
{
   getSignificance();

   // RooRealVar mass("mass", "invariant mass [GeV]", 125, 110, 160);
   // RooRealVar nSig("nSig", "number of signal events", 500, 0, 50000);
   // RooRealVar nBkg("nBkg", "number of background events", 100000, 0, 200000);

   // RooDataSet* signal = SignalToyMC(2000);
   // RooDataSet* data = BkgToyMC(100000);
   // // signal injection
   // data->append(*signal);

   // RooRealVar mean_Gauss("mean_Gauss","Mean of Gaussian [GeV]", 125., 110., 160.);
   // RooRealVar width_Gauss("width_Gauss","Width of Gaussian [GeV]", 3.0, 0.1, 5.);
   // RooGaussian signalShape("signalShape", "Gaussian PDF", mass, mean_Gauss, width_Gauss);

   // // RooRealVar mean_BreitWigner("mean_BreitWigner", "Mean of Breit-Wigner function [GeV]", 91.2);
   // // RooRealVar width_BreitWigner("width_BreitWigner", "width of the Breit-Wigner function [GeV]", 2.49);
   // // RooBreitWigner bkgShape("bkgShape", "Breit-Wigner function", mass, mean_BreitWigner, width_BreitWigner);

   // // Expo model
   // RooRealVar a1("a1", "Coefficient 1", 50., -500., 500.);
   // RooRealVar a2("a2", "Coefficient 2", -10., -500., 500.);
   // RooRealVar c("c", "Coefficient", 1.);
   // RooFormulaVar ZmumuShape("Zmumu", "-(a1*mass/100 + a2*(mass/100)^2)", RooArgList(a1, a2, mass));
   // RooExponential bkgShape("bkgShape", "background pdf", ZmumuShape, c);

   // RooAddPdf model("model", "S+B Model", RooArgList(signalShape, bkgShape), RooArgList(nSig, nBkg));

   // RooFitResult* fitResult = model.fitTo(*data, Extended(), Save());

   // RooPlot *plot = mass.frame();
   // data->plotOn(plot);
   // model.plotOn(plot, Components("signalShape"), LineStyle(kDashed), LineColor(kRed));
   // model.plotOn(plot, Components("bkgShape"), LineStyle(kDashed), LineColor(kBlue));
   // model.plotOn(plot);

   // TCanvas canvas("canvas");
   // plot->Draw();
   // canvas.SaveAs("combined_fit.pdf");

   // // Generate Asimov data
   // RooDataSet *data_asimov = model.generate(RooArgSet(mass), RooFit::Asimov(true));

   // RooAddPdf model1("model1", "signalShape + bkgShape", RooArgList(signalShape, bkgShape), RooArgList(nSig, nBkg));
   // RooAddPdf model0("model0", "bkgShape", RooArgList(bkgShape), RooArgList(nBkg));

   // RooAbsReal *nll0 = model0.createNLL(*data_asimov, RooFit::Extended(true));
   // RooAbsReal *nll1 = model1.createNLL(*data_asimov, RooFit::Extended(true));

   // std::cout << "nll0:" << nll0->getVal() << endl;
   // std::cout << "nll1:" << nll1->getVal() << endl;

   // // Compute test statistic q0
   // RooProduct nll0_times_two("nll0_times_two", "2*nll0", RooArgSet(*nll0, RooFit::RooConst(2)));
   // RooProduct nll1_times_two("nll1_times_two", "2*nll1", RooArgSet(*nll1, RooFit::RooConst(2)));

   // RooAddition q0("q0", "q0", RooArgList(nll1_times_two, nll0_times_two, RooFit::RooConst(-1)));

   // // Compute p-value
   // double p_value = ROOT::Math::chisquared_cdf_c(q0.getVal(), 1); // chi-square with 1 degree of freedom
}
