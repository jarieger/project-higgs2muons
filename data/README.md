# Theoretical background

## Higgs boson discovery

Particle physics describes the fundamental building blocks of matter and the forces that govern their interactions. The Large Hadron Collider (LHC) at CERN is the world's largest and most powerful particle accelerator. It is used to accelerate protons to extremely high energies and to collide them at four main detector experiments, known as ATLAS, CMS, ALICE and LHCb. Each of these detectors was built to study the properties of the particles produced in the proton-proton collisions. 

The discovery of a new particle can be a significant breakthrough in our understanding of the universe. For example, **the Higgs boson**, a particle that was first proposed in the 1960s to explain particle masses, was discovered with the ATLAS and CMS experiment about ten years ago. As particles produced in the proton-proton collisions decay very fast, it is not possible to directly detect them. However, by measuring the properties of the decay products, one can infer the properties of the original particle. For example, the energy and momentum of the decay products can be used to determine the mass of the original particle. 

Since the discovery of the Higgs boson many further postulated decay channels have been investigated. So far the interaction between the Higgs boson and the W and Z gauge bosons (force mediators), the heavy top and bottom quarks and the Tau-lepton could be observed. If the Higgs boson also interacts with lighter particles like the muon, remains to be one of the open questions in the field of particle physics for the moment.

## How particles aquire a mass & Why H->mu+mu- plays an important role..

All known elementary particles acquire their mass by interactions with the Higgs field. The Higgs boson is an excitation of this field. It manifests itself as an elementary particle with 0 spin (therefore it is called 'boson') and a mass of about 125 GeV. The heavier a particle, the stronger the interaction with the Higgs field. In other words, the Higgs boson decays into all particles with a mass and more frequently into heavy particles. Due to the small mass of the muon (106 MeV), the decay of the Higgs boson into a muon-antimuon pair is a very rare process. The ATLAS experiment at the LHC has been searching for this specific decay process in order to gain a deeper understanding of the hierarchy of particle masses observed in nature.

<!-- Why do particles have different mass? Why is there a huge difference between the electron mass (0.5 MeV) and the mass of the heaviest known particle, the top quark (173 GeV)? So far there are no answers to these questions. -->

Further information on the Higgs boson discovery and recent developments in this field can be found [here](https://atlas.cern/Discover/Physics/Higgs).
# Data analysis at the LHC

Simulated proton-proton collision data have been prepared to investigate the decay of a Higgs boson to muons. Compared to actual data, which has been taken by the ATLAS detector experiment, simulated data contains only one dedicated physical process. This allows for detailed investagtion of the properties of this process. Ideally these properties tell you something about what happened in the collision, and enable you to identify and distinguish different processes in the actual ATLAS data. This is important as you want to separate the physical processes you are interested in (**signal**) from other already known processes (**background**).

## Prepared samples of simulated events

The prepared simulated data contains the following signal processes:

- `GGF_h2muons.root` Gluon Fusion production of a Higgs boson and decay to muons (gg->H->mu+mu-),
- `VBF_h2muons.root` Vector Boson Fusion production of a Higgs boson and decay to muons (qq->Hqq->mu+mu- qq),

and a single background process:

- `DrellYan2muons.root` Drell-Yan production (qq->Z->mu+mu-).

To investiagte the properties of different physical processe, simulation and data files are typically organized in a data format referred to as **NTuple**, i.e., a number of measured properties of the particles emerging from the proton-proton collision for a certain amount of events. The signal NTuples contain 100k events. The background NTuple contains about 500k events. Please note, that this does not correspond to the actual ratio between signal and background events! 

Another important technical term often used by particle physicists is **Histogram**. A Histogram is a distribution of one property of a particle for the analyzed events, e.g., the measured transverse momentum of a muon in the simulated dataset.

The NTuples (".root" files) in this directory contain information on a set of different particle properties. A description of these properties and their labels (branch names) in the NTuple are summarized in this table:

| Branch name      | Description | 
| :---        |    :----:   |
| Muons_PT_Lead | pT value (in GeV) of highest pT muon in the event | 
| Muons_PT_Sub | pT value (in GeV) of second highest pT muon in the event | 
| Muons_Eta_Lead  | Eta angle value of highest pT muon in the event |
| Muons_Eta_Sub | Eta angle value of second highest pT muon in the event |
| Muons_Phi_Lead | Phi angle value of highest pT muon in the event |
| Muons_Phi_Sub | Phi angle value of second highest pT muon in the event |
| Muons_Charge_Lead | Charge value of highest pT muon in the event |
| Muons_Charge_Sub | Charge value of second highest pT muon in the event |
| Muons_Multip | Number of reconstructed muons in the event |
| Muons_Minv_MuMu | Reconstructed invariant mass of both muons (in GeV) |
| Muons_DeltaEta_MuMu | Difference in Eta angle between both muons |
| Muons_DeltaPhi_MuMu | Difference in Phi angle between both muons |
| Muons_DeltaR_MuMu | Distance between both muons |
| Muons_CosThetaStar | Angular property of both muons sensitve to the spin of the produced boson |
| Jets_E_Lead | Energy (in GeV) value of highest pT jet in the event |
| Jets_E_Sub | Energy (in GeV) of second highest pT jet in the event |
| Jets_PT_Lead | pT value (in GeV) of highest pT jet in the event |
| Jets_PT_Sub | pT value (in GeV) of second highest pT jet in the event |
| Jets_Eta_Lead | Eta angle value of highest pT jet in the event |
| Jets_Eta_Sub | Eta angle value of second highest pT jet in the event |
| Jets_Phi_Lead | Phi angle value of highest pT jet in the event |
| Jets_Phi_Sub | Phi angle value of second highest pT jet in the event |
| Jets_jetMultip | Number of jets in the event |
| Jets_PT_jj | Di-jet pT (pT value of four-momentum sum)|
| Jets_Eta_jj | Di-jet Eta (Eta value of four-momentum sum)|
| Jets_Phi_jj | Di-jet Phi (Phi value of four-momentum sum)|
| Jets_Minv_jj | Di-jet mass (mass value of four-momentum sum)|
| Jets_DeltaEta_jj | Angular separation between jets in longitudonal plane |
| Jets_DeltaPhi_jj | Angular separation between jets in transverse plane |
| Jets_DeltaR_jj | Distance between jets |
| Z_PT | Di-muon pT (pT value of four-momentum sum) |
| Z_Eta | Di-muon Eta (Eta value of four-momentum sum)|
| Z_Phi | Di-muon Phi (Phi value of four-momentum sum) |
| Z_Y | Di-muon Rapidity (rapidity value of four-momentum sum)|
| Event_Ht | Scalar sum of all reconstructed objects in the event (in GeV)|
| Event_MET | Missing transverse energy (in GeV)|
| Event_HasBJet | Event includes b-tagged jet (bool)|
| Truth_PT_Lead_Muon | Truth pT value (in GeV) of highest pT muon in the event |
| Truth_PT_Sub_Muon | Truth pT value (in GeV) of second highest pT muon in the event |
| Truth_Eta_Lead_Muon | Truth Eta angle value of highest pT muon in the event |
| Truth_Eta_Sub_Muon | Truth Eta angle value of second highest pT muon in the event |
| Truth_Phi_Lead_Muon | Truth Phi angle value of highest pT muon in the event |
| Truth_Phi_Sub_Muon | Truth Phi angle value of second highest pT muon in the event |
| Truth_Minv_MuMu | Truth invariant mass of both muons (in GeV)|

## TBrowser

To interactivly study the content of the NTuple, it's helpful to open the files with *ROOT*'s file browser. 

1. Open a terminal.
2. Navigate to the project area.
3. Initialise and open *ROOT*. Indicate the file you want to open when starting *ROOT*. For example: `root data/GGF_h2muons.root`.
4. Start a TBrowser `new TBrowser`.

A new window should pop up. Follow the instructions in this [document](../utils/instructions/TBrowser_Intro.pdf). Thereafter you are free to explore the files yourself.

**Proceed to [Chapter 1](../Chapter1/).**
